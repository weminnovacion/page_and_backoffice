<?php require('layout/header.php') ?>
    
    <header class="">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="index.php"><h2>WEMI</h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.php">Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php#more-info2">Sobre WEMI</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="services.php#single-services">Nuestros Servicios</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="contact.php#contact-information">Contactanos</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <?php require('layout/banner.php') ?>

    <div id="contact-information"></div>

    <div class="contact-information">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="contact-item">
              <i class="fa fa-whatsapp"></i>
              <h4>Whatsapp</h4>
              <p>Para Mensajes personalizados, contactenos y le responderemos en breve.</p>
              <a target="_blank" href="https://api.whatsapp.com/send?phone=3232328290&text=Hola%20estoy%20interesado%20en%20un%20desarrollo.">Contactanos</a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="contact-item">
              <i class="fa fa-phone"></i>
              <h4>Telefono</h4>
              <p>Para llamadas personalizadas, contactenos y le responderemos en breve.</p>
              <a href="tel: +57 0">Llamanos</a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="contact-item">
              <i class="fa fa-envelope"></i>
              <h4>Correo</h4>
              <p>Si tiene alguna pregunta, simplemente envie un correo y le responderemos en breve.</p>
              <a href="mailito:contacto@weminnovacion.com">contacto@weminnovacion.com</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php require('layout/form_contact.php') ?>

<?php require('layout/footer.php') ?>