

<?php require('layout/header.php') ?>

    <header class="">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="index.php"><h2>WEMI</h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Inicio
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.php#more-info2">Sobre WEMI</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="services.php#single-services">Nuestros Servicios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php#contact-information">Contactanos</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

  <?php require('layout/banner.php') ?>

  <div class="request-form">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <h4>Solicite una devolución de llamada ahora mismo.</h4>
          <span>Si tiene alguna pregunta, simplemente complete el formulario de contacto y le responderemos en breve.</span>
        </div>
        <div class="col-md-4">
          <a href="contact.php#contact-information" class="border-button">ponte en contacto</a>
        </div>
      </div>
    </div>
  </div>

  <div class="more-info">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div style="background-color: #ffffff;" class="more-info-content">
            <div class="row">
              <div class="col-md-6">
                <div class="left-image">
                  <img src="assets/images/about-image.jpg" alt="">
                </div>
              </div>
              <div class="col-md-6 align-self-center">
                <div class="right-content">
                  <span>Quienes somos</span>
                  <h2>Conozca acerca de <em>nuestra empresa</em></h2>
                  <p>Somos una empresa Colombiana, dedicada al progreso e innovación de tecnologías, diseño, mantenimiento, desarrollo de software y soporte de plataformas web, móviles y de escritorio, 
                  ofreciendo soluciones efectivas y eficaces a todas las empresas en el mundo.</p>
                  <a href="#" class="filled-button">Leer mas..</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="services">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-heading">
            <h2>WEMI <em>Innovación</em></h2>
            <span>Expertos en productos tecnologicos.</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service-item">
            <img src="assets/images/app-moviles.jpg" alt="">
            <div class="down-content">
              <h4>Desarrollo De Apps Móviles</h4>
              <p>Aplicaciones para Android, IOS y Hibridas.</p>
              <a href="" class="filled-button">Leer mas..</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service-item">
            <img src="assets/images/app_web_escritorio.jpg" alt="">
            <div class="down-content">
              <h4>Desarrollo De Apps Web</h4>
              <p>Páginas web, plataformas educativas, sectores empresariales.</p>
              <a href="" class="filled-button">Leer mas..</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service-item">
            <img src="assets/images/diseño_ui_ux.jpg" alt="">
            <div class="down-content">
              <h4>Diseño UI/UX</h4>
              <p>Implementamos experiencia de usuario en todos los proyectos que realizamos.</p>
              <a href="" class="filled-button">Leer mas..</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service-item">
            <img src="assets/images/consultoria_asesoria.jpg" alt="">
            <div class="down-content">
              <h4>Consultorías Y Asesorías</h4>
              <p>Ofrecemos seguimiento para la implementación de buenas prácticas al usar las TICs</p>
              <a href="" class="filled-button">Leer mas..</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="testimonials">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-heading">
            <h2>Lo que dicen <em>de nosotros</em></h2>
            <span>TESTIMONIOS DE NUESTROS MEJORES CLIENTES</span>
          </div>
        </div>
        <div class="col-md-12">
          <div class="owl-testimonials owl-carousel">
            <div class="testimonial-item">
              <div class="inner-content">
                <h4>George Walker</h4>
                <span>Chief Financial Analyst</span>
                <p>"Nulla ullamcorper, ipsum vel condimentum congue, mi odio vehicula tellus, sit amet malesuada justo sem sit amet quam. Pellentesque in sagittis lacus."</p>
              </div>
              <img src="http://placehold.it/60x60" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <?php require('layout/form_contact.php') ?>

<?php require('layout/footer.php') ?>
