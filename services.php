<?php require('layout/header.php') ?>
    
    <header class="">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="index.php"><h2>WEMI</h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.php">Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php#more-info2">Sobre WEMI</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="services.php#single-services">Nuestros Servicios</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php#contact-information">Contactanos</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <?php require('layout/banner.php') ?>

    <div id="single-services"></div>

    <div class="services">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-heading">
            <h2>WEMI <em>Innovación</em></h2>
            <span>Expertos en productos tecnologicos.</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service-item">
            <img src="assets/images/app-moviles.jpg" alt="">
            <div class="down-content">
              <h4>Desarrollo De Apps Móviles</h4>
              <p>Aplicaciones para Android, IOS y Hibridas.</p>
              <a href="" class="filled-button">Leer mas..</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service-item">
            <img src="assets/images/app_web_escritorio.jpg" alt="">
            <div class="down-content">
              <h4>Desarrollo De Apps Web</h4>
              <p>Páginas web, plataformas educativas, sectores empresariales.</p>
              <a href="" class="filled-button">Leer mas..</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service-item">
            <img src="assets/images/diseño_ui_ux.jpg" alt="">
            <div class="down-content">
              <h4>Diseño UI/UX</h4>
              <p>Implementamos experiencia de usuario en todos los proyectos que realizamos.</p>
              <a href="" class="filled-button">Leer mas..</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="service-item">
            <img src="assets/images/consultoria_asesoria.jpg" alt="">
            <div class="down-content">
              <h4>Consultorías Y Asesorías</h4>
              <p>Ofrecemos seguimiento para la implementación de buenas prácticas al usar las TICs</p>
              <a href="" class="filled-button">Leer mas..</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <?php require('layout/form_contact.php') ?>

<?php require('layout/footer.php') ?>

