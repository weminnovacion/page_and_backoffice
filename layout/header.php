<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="TemplateMo">
  <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
  <title>WEMI - Innovación y Desarrollo</title>
  <link rel="icon" href="assets/images/icono.ico" type="image/x-icon">
  <link href="assets/bootstrap/css/bootstrap.min.css?rv=<?= rand(1, 999); ?>" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/fontawesome.css?rv=<?= rand(1, 999); ?>">
  <link rel="stylesheet" href="assets/css/templatemo-finance-business.css?rv=<?= rand(1, 999); ?>">
  <link rel="stylesheet" href="assets/css/owl.css?rv=<?= rand(1, 999); ?>">
</head>

<body>

  <div id="preloader">
    <div class="jumper">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>

  <div class="sub-header">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-xs-12">
          <ul class="left-info">
            <li><a href="#"><i class="fa fa-clock-o"></i>Lun-Vie 07:00-17:00</a></li>
            <li>
              <a target="_blank" href="https://api.whatsapp.com/send?phone=3114718653&text=Hola%20estoy%20interesado%20en%20un%20desarrollo.">
                <i class="fa fa-whatsapp"></i>
                Whatsapp
              </a>
            </li>
            <li><a href="tel: +57 3114718653"><i class="fa fa-phone"></i>+57 3114718653</a></li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="right-icons">
            <li><a href="https://www.facebook.com/WEMInnovacion" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.instagram.com/weminnovacionydesarrollo" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCzY9XS0ThmxPhbyji09fqrw" target="_blank"><i class="fa fa-youtube"></i></a></li>
            <li><a href="https://twitter.com/wemi_y?s=08" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.linkedin.com/company/wemi-innovaci%C3%B3n-y-desarrollo" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="https://weminnovacion.com/app"><i class="fa fa-user"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>