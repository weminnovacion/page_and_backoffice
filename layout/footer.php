<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3 footer-item">
        <h4>WEMI INNOVACIÓN</h4>
        <p>WEMI es un equipo de diseñadores y desarrolladores de aplicaciones altamente experimentados que crean un software único para usted.</p>
        <ul class="social-icons">
          <li><a rel="nofollow" href="https://www.facebook.com/WEMInnovacion" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://www.instagram.com/weminnovacionydesarrollo" target="_blank"><i class="fa fa-instagram"></i></a></li>
          <li><a href="https://www.youtube.com/channel/UCzY9XS0ThmxPhbyji09fqrw" target="_blank"><i class="fa fa-youtube"></i></a></li>
          <li><a href="https://twitter.com/wemi_y?s=08" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://www.linkedin.com/company/wemi-innovaci%C3%B3n-y-desarrollo" target="_blank"><i class="fa fa-linkedin"></i></a></li>
        </ul>
      </div>
      <div class="col-md-1 footer-item"></div>
      <div class="col-md-4 footer-item">
        <h4>Nuestras especialidades</h4>
        <ul class="menu-list">
          <li><a href="#">Desarrollo Web</a></li>
          <li><a href="#">Desarrollo Móvil</a></li>
          <li><a href="#">Desarrollo Escritorio</a></li>
          <li><a href="#">Diseño UI/UX</a></li>
          <li><a href="#">Consultoria Y Asesorías</a></li>
        </ul>
      </div>
      <div class="col-md-2 footer-item">
        <h4>Otras paginas</h4>
        <ul class="menu-list">
          <li><a href="#">Trabaja con nosotros</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Soporte rápido</a></li>
          <li><a href="#">Política de privacidad</a></li>
        </ul>
      </div>
      <div class="col-md-2 footer-item">
        <h4>Ubicaciones</h4>
        <ul class="menu-list">
          <li><a href="#">Colombia, CO.</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>

<div class="sub-footer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p>Copyright &copy; 2019 WEMI Innovacion y Desarrollo.</p>
      </div>
    </div>
  </div>
</div>

<div id="fb-root"></div>
<div id="fb-customer-chat" class="fb-customerchat"></div>

<script>
  // var chatbox = document.getElementById('fb-customer-chat');
  // chatbox.setAttribute("page_id", "107404287786562");
  // chatbox.setAttribute("attribution", "biz_inbox");

  // window.fbAsyncInit = function() {
  //   FB.init({
  //     xfbml: true,
  //     version: 'v11.0'
  //   });
  // };

  // (function(d, s, id) {
  //   var js, fjs = d.getElementsByTagName(s)[0];
  //   if (d.getElementById(id)) return;
  //   js = d.createElement(s);
  //   js.id = id;
  //   js.src = 'https://connect.facebook.net/es_ES/sdk/xfbml.customerchat.js';
  //   fjs.parentNode.insertBefore(js, fjs);
  // }(document, 'script', 'facebook-jssdk'));
</script>

<script src="assets/jquery/jquery.min.js?rv=<?= rand(1, 999); ?>"></script>
<script src="assets/bootstrap/js/bootstrap.bundle.min.js?rv=<?= rand(1, 999); ?>"></script>
<script src="assets/js/custom.js?rv=<?= rand(1, 999); ?>"></script>
<script src="assets/js/owl.js?rv=<?= rand(1, 999); ?>"></script>
<script src="assets/js/slick.js?rv=<?= rand(1, 999); ?>"></script>
<script src="assets/js/accordions.js?rv=<?= rand(1, 999); ?>"></script>

<script language="text/Javascript">
  cleared[0] = cleared[1] = cleared[2] = 0;

  function clearField(t) {
    if (!cleared[t.id]) {
      cleared[t.id] = 1;
      t.value = '';
      t.style.color = '#fff';
    }
  }

  $(document).ready(function() {

  });
</script>
</body>

</html>