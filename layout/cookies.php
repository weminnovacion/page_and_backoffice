<div id="cajacookies">
    <p>
        <button onclick="aceptarCookies()" class="pull-right">
        <i class="fa fa-times">
        </i> Aceptar y cerrar éste mensaje
    </button>
    Éste sitio web usa cookies, si permanece aquí acepta su uso.
    Puede leer más sobre el uso de cookies en nuestra <a href="politica.html">política de privacidad</a>.
    </p>
</div>

