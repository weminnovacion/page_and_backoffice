
<?php require('layout/header.php') ?>

    <header class="">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="index.php"><h2>WEMI</h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.php">Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="about.php#more-info2">Sobre WEMI</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="services.php#single-services">Nuestros Servicios</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php#contact-information">Contactanos</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <?php require('layout/banner.php') ?>

    <div id="more-info2"></div>

    <div class="more-info">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div style="background-color: #ffffff;" class="more-info-content">
              <div class="row">
                <div class="col-md-6">
                  <div class="left-image">
                    <img src="assets/images/about-image.jpg" alt="">
                  </div>
                </div>
                <div class="col-md-6 align-self-center">
                  <div class="right-content">
                    <span>Quienes somos</span>
                    <h2>Conozca acerca de <em>nuestra empresa</em></h2>
                    <p>Somos una empresa Colombiana, dedicada al progreso e innovación de tecnologías, diseño, mantenimiento, desarrollo de software y soporte de plataformas web, móviles y de escritorio, 
                    ofreciendo soluciones efectivas y eficaces a todas las empresas en el mundo.</p>
                    <a href="#" class="filled-button">Leer mas..</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="more-info about-info">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="more-info-content">
              <div class="row">
                <div class="col-md-6 align-self-center">
                  <div class="right-content">
                    <h2>Nuestras soluciones para el <em>crecimiento de su negocio</em></h2>
                    <p>En WEMI te ofrecemos soluciones tecnológicas para tu compañía regulada y no regulada. Nuestra razón de ser se basa en brindar soluciones integrales de diseño y desarrollo a la medida, generando plataformas tecnológicas que responden a sus deseos y necesidades.</p>
                    <a href="#" class="filled-button">Leer mas..</a>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="left-image">
                    <img src="assets/images/cresimiento.jpg" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- <div class="team">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>Los miembros de<em> nuestro equipo</em></h2>
            </div>
          </div>
          <div class="col-md-4">
            <div class="team-item">
              <img src="assets/images/team_01.jpg" alt="">
              <div class="down-content">
                <h4>William Smith</h4>
                <span>Co-Founder</span>
                <p>In sem sem, dapibus non lacus auctor, ornare sollicitudin lacus. Aliquam ipsum urna, semper quis.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="team-item">
              <img src="assets/images/team_02.jpg" alt="">
              <div class="down-content">
                <h4>Mary Houston</h4>
                <span>Chief Marketing Officer</span>
                <p>In sem sem, dapibus non lacus auctor, ornare sollicitudin lacus. Aliquam ipsum urna, semper quis.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="team-item">
              <img src="assets/images/team_03.jpg" alt="">
              <div class="down-content">
                <h4>Paul Walker</h4>
                <span>Financial Analyst</span>
                <p>In sem sem, dapibus non lacus auctor, ornare sollicitudin lacus. Aliquam ipsum urna, semper quis.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <div class="testimonials">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>Lo que dicen <em>de nosotros</em></h2>
              <span>TESTIMONIOS DE NUESTROS MEJORES CLIENTES</span>
            </div>
          </div>
          <div class="col-md-12">
            <div class="owl-testimonials owl-carousel">
              <div class="testimonial-item">
                <div class="inner-content">
                  <h4>George Walker</h4>
                  <span>Chief Financial Analyst</span>
                  <p>"Nulla ullamcorper, ipsum vel condimentum congue, mi odio vehicula tellus, sit amet malesuada justo sem sit amet quam. Pellentesque in sagittis lacus."</p>
                </div>
                <img src="http://placehold.it/60x60" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php require('layout/footer.php') ?>