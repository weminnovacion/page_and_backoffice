$(document).ready(function () {
    $("#validName").hide()

    getMenu()
    getLink()
    getHeader()
});

function saveIcon() {
    var formData = new FormData();
    formData.append('foto', $('#icon')[0].files[0]);

    $.ajax({
        url: rutaUniversal + 'wemi/post_icon',
        data: formData,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function (data) {
            var res = JSON.parse(data)

            if (res.status === 'success') {
                $.notify({
                    title: '<strong>Exito</strong>',
                    message: res.message
                }, {
                    type: 'success',
                    delay: 5000,
                });
            } else {
                $.notify({
                    title: '<strong>Error</strong>',
                    message: res.message
                }, {
                    type: 'danger',
                    delay: 5000,
                });
            }
        }
    });
}

function getLink() {
    var maxField = 10;
    var addButton = $('.add_button');
    var wrapper = $('.field_wrapper');
    var fieldHTML = `
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group"> 
                        <label class="floating-label" for="links">Link</label>
                        <input type="text" name="link" id="link" class="form-control" placeholder="icon:link.com">
                    </div>
                </div>
            </div>
            <a tyle="text-align: end;" href="javascript:void(0);" class="remove_button"title="Remove field">Elimar <i class="feather icon-trash-2"></i></a>
        </div>`;
    var x = 1;

    $(addButton).click(function () {
        if (x < maxField) {
            x++;
            $(wrapper).append(fieldHTML);
        }
    });

    $(wrapper).on('click', '.remove_button', function (e) {
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    });
}

function getMenu() {
    var maxField = 10;
    var addButton = $('.add_button2');
    var wrapper = $('.field_wrapper2');
    var fieldHTML = `
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group"> 
                        <label class="floating-label" for="menu">Menu</label>
                        <input type="text" name="menu" id="menu" class="form-control" placeholder="name:link.com">
                    </div>
                </div>
            </div>
            <a tyle="text-align: end;" href="javascript:void(0);" class="remove_button2"title="Remove field">Elimar <i class="feather icon-trash-2"></i></a>
        </div>`;
    var x = 1;

    $(addButton).click(function () {
        if (x < maxField) {
            x++;
            $(wrapper).append(fieldHTML);
        }
    });

    $(wrapper).on('click', '.remove_button2', function (e) {
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    });
}

function getHeader() {
    $.ajax({
        url: rutaUniversal + 'wemi/getHeader',
        type: 'POST',
        data: { token: localStorage.getItem('token') },
        success: function (data) {
            var res = JSON.parse(data)

            if (res.status === 'success') {

                if (res.data) {
                    $('#name').val(res.data.headers[0].name)
                    $('#hour').val(res.data.headers[0].hour)
                    $('#phone').val(res.data.headers[0].phone)
                    $('#whatsapp').val(res.data.headers[0].whatsapp)
                }

                if (res.data && res.data.links.length > 0) {
                    var wrapper = $('.field_wrapper').html('');
                    var fieldHTML = "";
                    res.data.links.map((e) => {
                        fieldHTML += `
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                        <label class="floating-label" for="links">Link</label>
                                        <input type="text" name="link" id="link" class="form-control" value="${e.link}" placeholder="Ejemplo: icon;link.com">
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="remove_button" title="Remove field">Elimar <i class="feather icon-trash-2"></i></a>
                        </div>`;
                    })

                    $(wrapper).append(fieldHTML)
                } else if (links != null) {
                    $('#link').val(links[0].value)
                }

                if (res.data && res.data.menus.length > 0) {
                    var wrapper = $('.field_wrapper2').html('');
                    var fieldHTML = "";
                    res.data.menus.map((e) => {
                        fieldHTML += `
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group"> 
                                        <label class="floating-label" for="menu">Link</label>
                                        <input type="text" name="menu" id="menu" class="form-control" value="${e.value}" placeholder="Ejemplo: icon;link.com">
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="remove_button2" title="Remove field">Elimar <i class="feather icon-trash-2"></i></a>
                        </div>`;
                    })

                    $(wrapper).append(fieldHTML)
                } else if (links != null) {
                    $('#menu').val(links[0].value)
                }
            } else {

                $port.children('.spinner-border').hide();
                $port.children('.spinner-grow').hide();
                $port.children('.load-text').hide();
                $port.children('.btn-text').show();
                $port.removeAttr('disabled');

                $.notify({
                    title: '<strong>Error</strong>',
                    message: res.message
                }, {
                    type: 'danger',
                    delay: 5000,
                });
            }
        }
    }).fail(function () {

        $port.children('.spinner-border').hide();
        $port.children('.spinner-grow').hide();
        $port.children('.load-text').hide();
        $port.children('.btn-text').show();
        $port.removeAttr('disabled');

        $.notify({
            title: '<strong>Error de interno</strong>',
            message: 'Intente de nuevo mas tarde!'
        }, {
            type: 'danger',
            delay: 5000,
        });
    });
}

$("#icon").change(function() {
    var file = this.files[0];
    var imagefile = file.type;
    var match= ["image/jpeg","image/png","image/jpg"];
    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
        alert('Please select a valid image file (JPEG/JPG/PNG).');
        $("#icon").val('');
        return false;
    }
});

$('#event-btn2').on('click', function () {
    var $port = $(this);
    $port.children('.spinner-border').show();
    $port.children('.spinner-grow').show();
    $port.children('.load-text').show();
    $port.children('.btn-text').hide();
    $port.attr('disabled', 'true');

    var links = new Array();
    $('#columnas_excel').find('.form-group').each(function () {
        if ($(this).find('input').val() != "") {
            links.push({
                value: $(this).find('input').val()
            })
        }
    })

    var menus = new Array();
    $('#columnas_excel2').find('.form-group').each(function () {
        if ($(this).find('input').val() != "") {
            menus.push({
                value: $(this).find('input').val()
            })
        }
    })

    var name = $('#name').val()
    var hour = $('#hour').val()
    var whatsapp = $('#whatsapp').val()
    var phone = $('#phone').val()

    data = {
        token: localStorage.getItem('token'),
        name: name,
        hour: hour,
        whatsapp: whatsapp,
        phone: phone,
        links: links,
        menus: menus
    }

    console.log(data)

    if (validImputs()) {
        $.ajax({
            url: rutaUniversal + 'wemi/post_header',
            type: 'POST',
            data: data,
            success: function (data) {
                var res = JSON.parse(data)

                if (res.status === 'success') {

                    $port.children('.spinner-border').hide();
                    $port.children('.spinner-grow').hide();
                    $port.children('.load-text').hide();
                    $port.children('.btn-text').show();
                    $port.removeAttr('disabled');

                    $.notify({
                        title: '<strong>Exito</strong>',
                        message: res.message
                    }, {
                        type: 'success',
                        delay: 5000,
                    });

                } else {

                    $port.children('.spinner-border').hide();
                    $port.children('.spinner-grow').hide();
                    $port.children('.load-text').hide();
                    $port.children('.btn-text').show();
                    $port.removeAttr('disabled');

                    $.notify({
                        title: '<strong>Error</strong>',
                        message: res.message
                    }, {
                        type: 'danger',
                        delay: 5000,
                    });
                }
            }
        }).fail(function () {

            $port.children('.spinner-border').hide();
            $port.children('.spinner-grow').hide();
            $port.children('.load-text').hide();
            $port.children('.btn-text').show();
            $port.removeAttr('disabled');

            $.notify({
                title: '<strong>Error de interno</strong>',
                message: 'Intente de nuevo mas tarde!'
            }, {
                type: 'danger',
                delay: 5000,
            });
        });
    } else {
        $port.children('.spinner-border').hide();
        $port.children('.spinner-grow').hide();
        $port.children('.load-text').hide();
        $port.children('.btn-text').show();
        $port.removeAttr('disabled');
    }

})

$("#event-btn2").each(function () {
    $(this).children('.spinner-border').hide();
    $(this).children('.spinner-grow').hide();
    $(this).children('.load-text').hide();
});

function validImputs() {
    if (!$("#name").val()) {
        $("#validName").show()
        return false;
    } else {
        $("#validName").hide()
    }

    return true;
}