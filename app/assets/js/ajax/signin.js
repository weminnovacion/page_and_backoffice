var rutaUniversal = baseurl;

$(document).ready(function () {
    $("#validUsername").hide()
    $("#validPassword").hide()
})

$("#event-btn2").each(function () {
    $(this).children('.spinner-border').hide();
    $(this).children('.spinner-grow').hide();
    $(this).children('.load-text').hide();
});

function validImputs() {
    if (!$("#username").val()) {
        $("#validUsername").show()
        return false;
    } else {
        $("#validUsername").hide()
    }

    if (!$("#password").val()) {
        $("#validPassword").show()
        return false;
    } else {
        $("#validPassword").hide()
    }

    return true;
}

$('#event-btn2').on('click', function () {
    var $port = $(this);
    $port.children('.spinner-border').show();
    $port.children('.spinner-grow').show();
    $port.children('.load-text').show();
    $port.children('.btn-text').hide();
    $port.attr('disabled', 'true');

    if (validImputs()) {
        var username = $("#username").val()
        var password = $("#password").val()

        $.ajax({
            url: rutaUniversal + 'user/post_signin',
            type: 'POST',
            data: { 'username': username, 'password': password },
            success: function (data) {
                var res = JSON.parse(data)

                if (res.status === 'success') {

                    $port.children('.spinner-border').hide();
                    $port.children('.spinner-grow').hide();
                    $port.children('.load-text').hide();
                    $port.children('.btn-text').show();
                    $port.removeAttr('disabled');

                    localStorage.setItem("token", res.data)
                    window.location.href = rutaUniversal + "dashboard"
                } else {

                    $port.children('.spinner-border').hide();
                    $port.children('.spinner-grow').hide();
                    $port.children('.load-text').hide();
                    $port.children('.btn-text').show();
                    $port.removeAttr('disabled');

                    $.notify({
                        title: '<strong>Error</strong>',
                        message: res.message
                    }, {
                        type: 'danger',
                        delay: 5000,
                    });
                }
            }
        }).fail(function () {

            $port.children('.spinner-border').hide();
            $port.children('.spinner-grow').hide();
            $port.children('.load-text').hide();
            $port.children('.btn-text').show();
            $port.removeAttr('disabled');

            $.notify({
                title: '<strong>Error de interno</strong>',
                message: 'Intente de nuevo mas tarde!'
            }, {
                type: 'danger',
                delay: 5000,
            });
        });
    } else {
        $port.children('.spinner-border').hide();
        $port.children('.spinner-grow').hide();
        $port.children('.load-text').hide();
        $port.children('.btn-text').show();
        $port.removeAttr('disabled');
    }
});