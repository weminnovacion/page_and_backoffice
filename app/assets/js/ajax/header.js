var rutaUniversal = baseurl;

$(document).ready(function () {
    var location2 = window.location.toString();
    var name = location2.split("/").length >= 6 ? location2.split("/")[5].toUpperCase() : location2.split("/")[4].toUpperCase();
    var subName = location2.split("/").length >= 6 ? location2.split("/")[4].toUpperCase() : '';

    var menu = "<div class='page-block'";
        menu += "<div class='row align-items-center'";
        menu += "<div class='col-md-12'>";
        menu += "<div class='page-header-title'>";
        menu += `<h5 class="m-b-10">${name}</h5>`;
        menu += `</div>`;

        menu += `<ul class="breadcrumb">`;
        menu += `<li class="breadcrumb-item"><a><i class="feather icon-home"></i></a></li>`;
        if (location2.split("/").length >= 6) {
            menu += `<li class="breadcrumb-item"><a>${subName}</a></li>`;
            menu += `<li class="breadcrumb-item"><a>${name}</a></li>`;
        } else {
            menu += `<li class="breadcrumb-item"><a>${name}</a></li>`;
        }
        menu += `</ul>`;
        menu += `</div>`;
        menu += `</div>`;
        menu += `</div>`;

    $("#page-header").append(menu)
})

function getMenus() {
    var menu = '';

    $.ajax({
        url: rutaUniversal + 'header/index',
        type: 'POST',
        data: { token: localStorage.getItem('token') },
        success: function (data) {
            var res = JSON.parse(data)

            if (res.status === 'success') {
                $.each(res.data.arrayMenuFather, function (i, item) {
                    if (item.count > 0) {
                        menu += '<li class="nav-item pcoded-hasmenu">';
                        menu += '    <a href="#!" class="nav-link ">';
                        menu += '        <span class="pcoded-micon">';
                        menu += '            <i class="' + item.icon + '"></i>';
                        menu += '        </span>';
                        menu += '        <span class="pcoded-mtext">' + item.name + '</span>';
                        menu += '    </a>';
                        menu += '    <ul class="pcoded-submenu">';
                        $.each(res.data.arrayMenusChild, function (i, item2) {
                            if (item2.father_id == item.id) {
                                menu += '    <li><a href="' + rutaUniversal + item2.controller + '/' + item2.method + '">' + item2.name + '</a></li>';
                            }
                        })
                        menu += '    </ul>';
                        menu += '</li>';
                    } else {
                        menu += '<li class="nav-item">';
                        menu += '<a href="' + rutaUniversal + item.controller + '/' + item.method + '" class="nav-link "><span class="pcoded-micon"><i class="' + item.icon + '"></i></span><span class="pcoded-mtext">' + item.name + '</span></a>';
                        menu += '</li>';
                    }
                })

                $("#menu").append(menu);
            } else {
                $.notify({
                    title: '<strong>Error</strong>',
                    message: res.message
                }, {
                    type: 'danger',
                    delay: 5000,
                });
            }
        }
    }).fail(function () {
        $.notify({
            title: '<strong>Error de interno</strong>',
            message: 'Intente de nuevo mas tarde!'
        }, {
            type: 'danger',
            delay: 5000,
        });
    });
}

getMenus()