<!DOCTYPE html>
<html lang="en">

<head>
    <title>Signin - WEMI Innovación y Desarollo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/icono.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
</head>

<div class="auth-wrapper">
    <div class="auth-content text-center">
        <img src="<?php echo base_url(); ?>assets/images/logo1.png" alt="" style="width: 50%;" class="img-fluid mb-4">
        <div class="card borderless">
            <div class="row align-items-center ">
                <div class="col-md-12">
                    <div class="card-body">
                        <h4 class="mb-3 f-w-400">Iniciar sesión en WEMI</h4>
                        <hr>
                        <div class="form-group mb-3">
                            <input type="text" class="form-control" id="username" name="username" placeholder="Usuario">
                            <p id="validUsername" class="mb-2 text-muted" style="position: relative; right: 31%; color: red !important;">Campo requerido</p>
                        </div>
                        <div class="form-group mb-4">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                            <p id="validPassword" class="mb-2 text-muted" style="position: relative; right: 31%; color: red !important;">Campo requerido</p>
                        </div>
                        <div class="custom-control custom-checkbox text-left mb-4 mt-2">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">Guadar contraseña.</label>
                        </div>
                        <button class="btn btn-primary event-btn m-2" id="event-btn2" type="button">
                            <span class="spinner-grow spinner-grow-sm" role="status"></span>
							<span class="load-text">Cargando...</span>
							<span class="btn-text">Iniciar sesión</span>
						</button>
                        <!-- <hr> -->
                        <p class="mb-2 text-muted">Recuperar contraseña? <a href="#" class="f-w-400">Reiniciar</a></p>
                        <!-- <p class="mb-0 text-muted">No tienes una cuenta? <a href="auth-signup.html" class="f-w-400">Inscribirse</a></p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var baseurl = "<?php echo base_url(); ?>"
</script>

<script src="<?php echo base_url(); ?>assets/js/vendor-all.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ajax/signin.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>assets/notify/bootstrap-notify.js"></script>
<script src="<?php echo base_url(); ?>assets/notify/bootstrap-notify.min.js"></script>

</body>

</html>