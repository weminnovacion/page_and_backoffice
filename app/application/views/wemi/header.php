<style>
    .user {
        display: inline-block;
        width: 150px;
        height: 150px;
        border-radius: 50%;
        border-top: 3px solid #8CDDCD;
        border-bottom: 3px solid #8CDDCD;
        border-right: 3px solid #8CDDCD;
        border-left: 3px solid #8CDDCD;
        background-repeat: no-repeat;
        background-position: center center;
        background-size: cover;
    }

    .one {
        background-image: url('<?php echo base_url(); ?>assets/images/uploads/profile.jpg');
    }
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>WEMI HEADER</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <!-- <div class="user one"></div> -->
                    <!-- <div class="col-sm-4">
                        <div class="input-group cust-file-button">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="icon">
                                <label class="custom-file-label" for="icon">Seleccionar Icono</label>
                            </div>
                            <div class="input-group-append">
                                <button class="btn  btn-primary" onclick="saveIcon()" id="saveIcon" type="button">Enviar</button>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="row">
                            <div style="text-align: center;" class="col-sm-12">
                                <div style="top: 8%; position: relative;" class="user one"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="floating-label" for="name">Nombre Empresa</label>
                                    <input type="text" class="form-control" id="name" value="">
                                    <p id="validName" class="mb-2 text-muted" style="position: relative; color: red !important;">Campo requerido</p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="floating-label" for="hour">Horario Activo</label>
                                    <input type="text" class="form-control" id="hour" value="" placeholder="Lun-Vie 07:00-17:00">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="floating-label" for="password">Celular Empresa</label>
                                    <input type="text" class="form-control" id="phone" placeholder="+57 3114718653">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="floating-label" for="Text">Whatsapp Empresa</label>
                                    <textarea type="text" class="form-control" rows="1" id="whatsapp" placeholder="phone=3114718653&text=hola"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion" id="accordionExample">
                    <div class="card mb-0">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0"><a href="#!" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Redes sociales</a></h5>
                            <a style="text-align: end;" href="javascript:void(0);" style="top: -5px; position: relative;" class="add_button" title="Add field">Agregar Redes <i class="feather icon-plus-circle"></i></a>
                        </div>
                        <div id="collapseOne" class="collapse hidde" aria-labelledby="headingOne" data-parent="#accordionExample"> </br>
                            <div class="row field_wrapper" id="columnas_excel">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="floating-label" for="links">Link</label>
                                        <input type="text" class="form-control" name="link" id="link" placeholder="icon:link.com">
                                    </div>
                                </div>
                            </div>
                        </div></br>
                    </div></br>
                </div>
                <div class="accordion" id="accordionExample2">
                    <div class="card mb-0">
                        <div class="card-header" id="headingOne2">
                            <h5 class="mb-0"><a href="#!" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="true" aria-controls="collapseOne">Menus header</a></h5>
                            <a style="text-align: end;" href="javascript:void(0);" style="top: -5px; position: relative;" class="add_button2" title="Add field">Agregar Menu <i class="feather icon-plus-circle"></i></a>
                        </div>
                        <div id="collapseOne2" class="collapse hidde" aria-labelledby="headingOne2" data-parent="#accordionExample2"> </br>
                            <div class="row field_wrapper2" id="columnas_excel2">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="floating-label" for="menu">Menu</label>
                                        <input type="text" class="form-control" name="menu" id="menu" placeholder="name:link.com">
                                    </div>
                                </div>
                            </div>
                        </div></br>
                    </div></br>
                </div>
                <div id="submitStart" style="text-align: end;">
                    <button class="btn btn-primary event-btn m-2" id="event-btn2" type="button">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="load-text">Cargando...</span>
                        <span class="btn-text">Guardar</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/ajax/wemi/header.js?rv=<?= rand(1, 999); ?>"></script>