<style media="screen">
.breadcrumb-area {
  position: relative !important;
  z-index: 1 !important;
  height: 660px !important;
}
.single-ticket-pricing-table.style-2 {
    background-color: #ffffff !important;
    box-shadow: 0px 8px 27px 0px rgba(57, 113, 188, 0.71) !important;
}
.confer-btn-white {
    background: #1e3e7d;
    color: #ffffff;
	width: 100%;
}
.confer-btn-white:hover, .confer-btn-white:focus {
    background-color: #ffffff;
    /* border-color: #000000; */
    color: #000000;
	width: 100%;
}
.about-us-countdown-area {
    position: relative !important;
    z-index: 1 !important;
    padding: 50px !important;
}
</style>
<script>
   var baseurl = "<?php echo base_url(); ?>"
</script>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>
<!-- Breadcrumb Area Start -->
<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/wp2044198.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">CAB CAPITAL</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Servicios</li>
                            <li class="breadcrumb-item active" aria-current="page">Cab Capital</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->