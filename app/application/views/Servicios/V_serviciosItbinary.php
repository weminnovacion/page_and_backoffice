<style media="screen">
.breadcrumb-area {
  position: relative !important;
  z-index: 1 !important;
  height: 660px !important;
}
.confer-btn-white {
    background: #1e3e7d;
    color: #ffffff;
	width: 100%;
}
.confer-btn-white:hover, .confer-btn-white:focus {
    background-color: #ffffff;
    /* border-color: #000000; */
    color: #000000;
	width: 100%;
}
.about-us-countdown-area {
    padding: 50px !important;
}
p.tagets {
    color: #434848 !important;
}
h5.tagets {
    color: #0a3e7d !important;
}
</style>
<script>
   var baseurl = "<?php echo base_url(); ?>"
</script>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>
<!-- Breadcrumb Area Start -->
<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/emprendedor3.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">IT BINARY</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Servicios</li>
                            <li class="breadcrumb-item active" aria-current="page">IT BINARY</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->

<!-- What We offer Area Start -->
<!-- About Us And Countdown Area Start -->
<section class="about-us-countdown-area section-padding-100-0"  id="about">

    <div class="container">
        <div class="row align-items-center">
            <!-- About Thumb -->
            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/binary.com.png" alt="">
                </div>
            </div>
            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80">
                    <h6 class="wow fadeInUp" style="color:#062a54 !important" data-wow-delay="300ms">Servicios Automaticos</h6>
                    <h3 class="wow fadeInUp" style="color:#062a54" data-wow-delay="300ms">IT BINARY</h3>
                    <p style="text-align: center;" class="wow fadeInUp" data-wow-delay="300ms"><a style="font-size:20px;"> ¡Operación en línea con Binary.com! </a> 
                    <p style="text-align: justify;" class="wow fadeInUp" data-wow-delay="300ms"> Hacemos tu vida más fácil, somos creadores de estrategias y las automatizamos para que negocies más eficientemente de lo que creías posible.
                      <br> <a style="font-size:20px;color:#062a54">¿Qué son las opciones binarias?</a>
                      <p style="text-align: justify;" class="wow fadeInUp" data-wow-delay="300ms">Una opción binaria es un tipo de opción con ganancia fija en la que se realiza una predicción sobre dos posibles resultados. Si la predicción del inversionista es correcta, recibe el pago predeterminado. En caso contrario, pierde solamente su inversión inicial. Se les llama opciones "binarias" porque solo puede haber dos resultados: GANAR o PERDER.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us And Countdown Area End -->

<section class="what-we-offer-area section-padding-100-70">
    <div class="container">
        <div class="row">
            <!-- Heading -->
            <div class="col-12">
                <div class="section-heading-3 text-center wow fadeInUp" data-wow-delay="300ms">
                    <h4>IT BINARY</h4>
                    <p>Modelo de inversion</p>
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col-12 col-md-12">
             
           </div>
            <!-- Single We Offer Area -->
            <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/mercado.png" alt="">
                    </div>
                    <h5 class="tagets">⦁	Todos los mercados</h5>
                    <p class="tagets">Opere con mercados subyacentes que incluyen Forex, índices, materias primas y mucho más.</p>
                </div>
            </div>

            <!-- Single We Offer Area -->
            <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/estadistica.png" alt="">
                    </div>
                    <h5 class="tagets">⦁	Todas las condiciones de mercado</h5>
                    <p class="tagets">Prediga los movimientos del mercado usando operaciones de tipo arriba/abajo, toque/sin toque y dentro/fuera.</p>
                </div>
            </div>
            <!-- Single We Offer Area -->
            <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/calendario.png" alt="">
                    </div>
                    <h5 class="tagets">⦁	Todas las duraciones</h5>
                    <p class="tagets">Escoja una estrategia a corto o largo plazo con operaciones que van desde 10 segundos hasta 365 días.</p>
                </div>
            </div>
            <!-- Single We Offer Area -->
            <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pago.png" alt="">
                    </div>
                    <h5 class="tagets">⦁	Todos los pagos</h5>
                    <p class="tagets">Reciba pagos de hasta USD 50.000. Las pérdidas solo están limitadas a su apuesta inicial.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section style="background:white !important;" id="Sectionpagos" class="about-us-countdown-area section-padding-100-0">
<div class="container">
      <div class="row">
   <div class="col-12 col-md-6 col-xl-6">
        <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
            <!-- Icon -->
            <div class="offer-icon">
                <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/logoPaypal.png" alt="">
            </div>
            <h5>Paga con Paypal</h5>                    
            <div class="col-12">
                <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                    <a  class="btn confer-btn-white" href="<?= site_url('C_registro/itbinary'); ?>" ?>Pagar <i class="zmdi zmdi-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-12 col-md-s6 col-xl-6">
        <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
            <!-- Icon -->
            <div class="offer-icon">
                <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/Otropago.png" alt="">
            </div>
            <h5>Pagos por BTC</h5>                    
            <div class="col-12">
                <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                    <a class="btn confer-btn-white" href="<?= site_url('C_registro/Btcitbinary'); ?>"  >Pagar <i class="zmdi zmdi-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>    

  </div>
</div>    
</section>
