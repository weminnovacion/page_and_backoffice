<style media="screen">
.single-ticket-pricing-table.style-2 {
  background-color: #ffffff;
  box-shadow: 0px 8px 27px 0px rgba(57, 113, 188, 0.71);
}
p {
    line-height: 1.8;
    color: #000000;
    font-size: 16px;
    font-weight: 400;
}
.breadcrumb-area {
  position: relative !important;
  z-index: 1 !important;
  height: 660px !important;
}
.confer-btn-white {
    background: #1e3e7d;
    color: #ffffff;
	width: 100%;
}
.confer-btn-white:hover, .confer-btn-white:focus {
    background-color: #ffffff;
    /* border-color: #000000; */
    color: #000000;
	width: 100%;
}
section#Sectionpagos {
    padding: 50px !important;
}
</style>
<script>
   var baseurl = "<?php echo base_url(); ?>"
</script>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>
<!-- Breadcrumb Area Start -->
<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/wp2044217.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">FX TRADE</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Servicios</li>
                            <li class="breadcrumb-item active" aria-current="page">FX TRADE</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->

<!-- What We offer Area Start -->
<!-- About Us And Countdown Area Start -->
<section class="about-us-countdown-area section-padding-100-0"  id="about">

    <div class="container">
        <div class="row align-items-center">
            <!-- About Thumb -->
            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/fxtrade.png" alt="">
                </div>
            </div>
            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80 single-ticket-pricing-table style-2 wow fadeInUp"><br>
                    <h6 class="wow fadeInUp" style="color:#062a54 !important" data-wow-delay="300ms">Servicios Automaticos</h6>
                    <h3 class="wow fadeInUp" style="color:#062a54" data-wow-delay="300ms">FX TRADE</h3>
                    <p class="wow fadeInUp" style="color:black" data-wow-delay="300ms">En ITC ofrecemos un servicio especializado en trading automático en FOREX.
                      <br> <a style="font-size:16px;color:#062a54">¿Qué es Forex?</a> Mercado financiero con mayor liquidez del mundo.
                       Compra y venta de divisas, 24 horas al día/ 5 días a la semana.
                      Operaciones desde cualquier dispositivo móvil. Circulan más de 5.3 trillones de dólares diarios. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us And Countdown Area End -->

<section class="what-we-offer-area section-padding-100-70">
    <div class="container">
    <div class="row">
            <!-- Heading -->
            <div class="col-12">
                <div class="section-heading-3 text-center wow fadeInUp" data-wow-delay="300ms">
                    <h4>FX TRADE</h4>
                    <p>Modelo de inversion</p>
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col-12 col-md-12">
              
           </div>
            <!-- Single We Offer Area -->
            <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer-icon">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/robotmenor.png" alt="">
                    </div>
                    <h5>Bot FxTrade</h5>
                    <p>Realiza operaciones scalping con coberturas de forma conservadora y coloca TP, sigue la tendencia y en el retroceso cierra en ganancia. Opera 24 Horas, 5 días a la semana.</p>
                </div>
            </div>

            <!-- Single We Offer Area -->
            <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer-icon">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/robotmayor.png" alt="">
                    </div>
                    <h5>Bot Fenix2.0</h5>
                    <p>Opera en la apertura del mercado asiático durante 1 a 3 horas, con efectividad del 98%. Coloca TP y SL, tiene inteligencia artificial y cierra la operación al reflejarse ganancia.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-us-countdown-area section-padding-100-0">
    <div class="container">
      <div class="row">
          <!-- Heading -->
          <div class="col-12">
              <div class="section-heading-3 text-center wow fadeInUp" data-wow-delay="300ms">
                  <h4 style="color:white">Caracteristicas del servicio FX TRADE</h4>
              </div>
          </div>
      </div>
        <div class="row">
<!-- Post Author Area -->
<div class="col-12 col-md-4 col-xl-4">
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 active text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/billeteras.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5></h5>
                    <p>1. Mantienes control total del dinero, tienes tu propia cuenta. </p>
                </div>
            </div>
            </div>
            <div class="col-12 col-md-4 col-xl-4">
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/signo.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5></h5>
                    <p>2. Nuestro riesgo de operación es bajo, ajustado a tu capital.</p>
                </div>
            </div>
            </div>
            <div class="col-12 col-md-4 col-xl-4">
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/dinero.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5></h5>
                    <p>3. Seguimiento de cuentas por traders profesionales.</p>
                </div>
            </div>
            </div>
            <div class="col-12 col-md-4 col-xl-4">
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/4caract.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5></h5>
                    <p>4. Broker certificado que brinda seguridad: HOTFOREX </p>
                </div>
            </div>
            </div>
            <div class="col-12 col-md-4 col-xl-4">
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/5caract.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5></h5>
                    <p>5. Flexibilidad en depósitos y retiros. Donde y cuando quieras! </p>
                </div>
            </div>
            </div>
            <div class="col-12 col-md-4 col-xl-4">
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/dinero2.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5></h5>
                    <p>6. Rentabilidad Mínima de un 20% al mes.</p>
                </div>
            </div>
            </div>
          </div>
      </div>

      <div style="padding-top: 30px;background:#3971bd" class="our-blog-area section-padding-100">
    <div class="container">
        <div class="row">
          <div class="col-12">
              <div class="section-heading-2 text-center wow fadeInUp" data-wow-delay="300ms">
                  
                  <h4>PAQUETES</h4>
                  <p style="color:white">Servicio Trading Automático</p>
              </div>
          </div>
                <div class="col-12 col-md-6 col-xl-4">

                    <div style="background-color:#3971bc" class="single-blog-area style-2 wow fadeInUp" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin1.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">Starter</a>
                            
                            <h2 class="ticket-price"><span>$</span>250 USD</h2>
                            <div class="ticket-pricing-table-details">
                                <p><a class="post-author" href="#"><i class="zmdi zmdi-money"></i> Rango de Capitales:</p>
                                <p><i class="zmdi zmdi-check"></i> Min: $ 1.000 USD</p>
                                <p><i class="zmdi zmdi-check"></i> Max: $ 4.999 USD</p><br>
                            </div>
                            <!-- Post Meta -->
                            <div>
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i> Trading Output: 12 Meses</a>
                            </div>
                            <div>
                                <a class="post-author" href="#"><i class="zmdi zmdi-calendar"></i> Monthly Payment: 100$</a>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <!-- Single Blog Area -->
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="single-blog-area style-2 wow fadeInUp" style="background-color:#3971bc" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin1.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">Basic Trader</a>
                            
                            <h2 class="ticket-price"><span>$</span>350 USD</h2>
                            <div class="ticket-pricing-table-details">
                            <p><a class="post-author" href="#"><i class="zmdi zmdi-money"></i> Rango de Capitales:</p>
                                <p><i class="zmdi zmdi-check"></i> Min: $ 5.000 USD</p>
                                <p><i class="zmdi zmdi-check"></i> Max: $ 9.999 USD</p><br>
                            </div>
                            <!-- Post Meta -->
                            <div>
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i> Trading Output: 12 Meses</a>
                            </div>
                            <div>
                                <a class="post-author" href="#"><i class="zmdi zmdi-calendar"></i> Monthly Payment: 100$</a>
                            </div>
                        </div>
                    </div>
                </div>

                 <!-- Single Blog Area -->
                 <div class="col-12 col-md-6 col-xl-4">
                    <div class="single-blog-area style-2 wow fadeInUp" style="background-color:#3971bc" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin1.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">Pro Trader</a>
                            
                            <h2 class="ticket-price"><span>$</span>450 USD</h2>
                            <div class="ticket-pricing-table-details">
                            <p><a class="post-author" href="#"><i class="zmdi zmdi-money"></i> Rango de Capitales:</p>
                                <p><i class="zmdi zmdi-check"></i> Min: $ 10.000 USD</p>
                                <p><i class="zmdi zmdi-check"></i> Max: $ 19.999 USD</p><br>
                            </div>
                            <!-- Post Meta -->
                            <div>
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i> Trading Output: 12 Meses</a>
                            </div>
                            <div>
                                <a class="post-author" href="#"><i class="zmdi zmdi-calendar"></i> Monthly Payment: 150$</a>
                            </div>
                        </div>
                    </div>
                </div>
             <!-- Single Blog Area -->
             <div class="col-12 col-md-6 col-xl-4">
                    <div class="single-blog-area style-2 wow fadeInUp" style="background-color:#3971bc" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin1.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">Premium Trader</a>
                            
                            <h2 class="ticket-price"><span>$</span>550 USD</h2>
                            <div class="ticket-pricing-table-details">
                            <p><a class="post-author" href="#"><i class="zmdi zmdi-money"></i> Rango de Capitales:</p>
                                <p><i class="zmdi zmdi-check"></i> Min: $ 20.000 USD</p>
                                <p><i class="zmdi zmdi-check"></i> Max: $ 39.999 USD</p><br>
                            </div>
                            <!-- Post Meta -->
                            <div>
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i> Trading Output: 12 Meses</a>
                            </div>
                            <div>
                                <a class="post-author" href="#"><i class="zmdi zmdi-calendar"></i> Monthly Payment: 200$</a>
                            </div>
                        </div>
                    </div>
                </div>
                 <!-- Single Blog Area -->
                 <div class="col-12 col-md-6 col-xl-4">
                    <div class="single-blog-area style-2 wow fadeInUp" style="background-color:#3971bc" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin1.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">Master Trader</a>
                            
                            <h2 class="ticket-price"><span>$</span>650 USD</h2>
                            <div class="ticket-pricing-table-details">
                            <p><a class="post-author" href="#"><i class="zmdi zmdi-money"></i> Rango de Capitales:</p>
                                <p><i class="zmdi zmdi-check"></i> Min: $ 40.000 USD</p>
                                <p><i class="zmdi zmdi-check"></i> Max: $ 49.999 USD</p><br>
                            </div>
                            <!-- Post Meta -->
                            <div>
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i> Trading Output: 12 Meses</a>
                            </div>
                            <div>
                                <a class="post-author" href="#"><i class="zmdi zmdi-calendar"></i> Monthly Payment: 250$</a>
                            </div>
                        </div>
                    </div>
                </div>
                 <!-- Single Blog Area -->
                 <div class="col-12 col-md-6 col-xl-4">
                    <div class="single-blog-area style-2 wow fadeInUp" style="background-color:#3971bc" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin1.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">Elite Trader</a>
                            
                            <h2 class="ticket-price"><span>$</span>800 USD</h2>
                            <div class="ticket-pricing-table-details">
                            <p><a class="post-author" href="#"><i class="zmdi zmdi-moneyc"></i> Rango de Capitales:</p>
                                <p><i class="zmdi zmdi-check"></i> Min: $ 50.000 USD</p>
                                <p><i class="zmdi zmdi-check"></i> Max: $100.000 USD</p><br>
                            </div>
                            <!-- Post Meta -->
                            <div>
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i> Trading Output: 12 Meses</a>
                            </div>
                            <div>
                                <a class="post-author" href="#"><i class="zmdi zmdi-calendar"></i> Monthly Payment: 300$</a>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div>

</section>
<section style="background:white !important;" id="Sectionpagos" class="about-us-countdown-area section-padding-100-0">
  <div class="container">
      <div class="row">
            <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer-icon">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/logoPaypal.png" alt="">
                    </div>
                    <h5>Paga con Paypal</h5>                    
					<div class="col-12">
                        <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                            <a  class="btn confer-btn-white" href="<?= site_url('C_registro/Fxtrade'); ?>" >Pagar <i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
			
            <div class="col-12 col-md-s6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer-icon">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/Otropago.png" alt="">
                    </div>
                    <h5>Pagos por BTC</h5>                    
				   <div class="col-12">
                        <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                            <a class="btn confer-btn-white" href="<?= site_url('C_registro/BtcFxtrade'); ?>"  >Pagar <i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>     
                  
		</div>
     </div>    
</section>
