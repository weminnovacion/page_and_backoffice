<style media="screen">
.breadcrumb-area {
  position: relative !important;
  z-index: 1 !important;
  height: 660px !important;
}
.single-ticket-pricing-table.style-2 {
    background-color: #ffffff !important;
    box-shadow: 0px 8px 27px 0px rgba(57, 113, 188, 0.71) !important;
}
.confer-btn-white {
    background: #1e3e7d;
    color: #ffffff;
	width: 100%;
}
.confer-btn-white:hover, .confer-btn-white:focus {
    background-color: #ffffff;
    /* border-color: #000000; */
    color: #000000;
	width: 100%;
}
.about-us-countdown-area {
    position: relative !important;
    z-index: 1 !important;
    padding: 50px !important;
}
</style>
<script>
   var baseurl = "<?php echo base_url(); ?>"
</script>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>
<!-- Breadcrumb Area Start -->
<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/wp2044198.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">IT Manager</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Servicios</li>
                            <li class="breadcrumb-item active" aria-current="page">IT Manager</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->

<!-- What We offer Area Start -->
<!-- About Us And Countdown Area Start -->
<section class="about-us-countdown-area section-padding-100-0"  id="about">

    <div class="container">
        <div class="row align-items-center">
            <!-- About Thumb -->
            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pamm-score1.png" alt="">
                </div>
            </div>
            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80">
                    <h6 class="wow fadeInUp" style="color:#062a54 !important" data-wow-delay="300ms">Servicios Automaticos</h6>
                    <h3 class="wow fadeInUp" style="color:#062a54" data-wow-delay="300ms">IT Manager</h3>
                    <p class="wow fadeInUp" data-wow-delay="300ms">El servicio IT Manager, creado por ITC, es un producto único en su clase que goza de popularidad a nivel mundial. Permite a inversores minoristas invertir de forma segura, ya que permite hacer una mejor gestión del capital, sin riesgo.
                      <br> <a style="font-size:20px;color:#062a54">¿Que Son las cuentas IT Manager?</a> Una cuenta IT Manager es una fórmula de inversión colectiva que ofrece importantes ventajas para aquellos inversores con menos conocimientos sobre los mercados financieros o que simplemente prefieren aprovecharse de la experiencia de otros traders para obtener beneficios. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us And Countdown Area End -->

<section class="what-we-offer-area section-padding-100-70">
    <div class="container">
        <div class="row">
            <!-- Heading -->
            <div class="col-12">
                <div class="section-heading-3 text-center wow fadeInUp" data-wow-delay="300ms">
                    <h4>IT Manager</h4>
                    <p>Modelo de inversion</p>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Single We Offer Area -->
            <div class="col-12 col-md-6 col-xl-4">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer-icon">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/moneymanager.png" alt="">
                    </div>
                    <h5>Gestor de la cuenta</h5>
                    <p>En ITC contamos con una cuenta IT Manager con capital propio, y nos ofrecemos a gestionar todas las operaciones en base a nuestras herramientas de trading automático con minimos riesgos que manejamos para su capital.</p>
                </div>
            </div>

            <!-- Single We Offer Area -->
            <div class="col-12 col-md-6 col-xl-4">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer-icon">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/investor2.png" alt="">
                    </div>
                    <h5>Inversores</h5>
                    <p>Una serie de inversores individuales se asocian a la cuenta IT Manager creada por ITC y depositan el capital que consideren oportuno con el objetivo de obtener beneficios de las operaciones realizadas por ITC de la cuenta IT Manager.</p>
                </div>
            </div>

            <!-- Single We Offer Area -->
            <div class="col-12 col-md-6 col-xl-4">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer-icon">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/banco.png" alt="">
                    </div>
                    <h5>Broker</h5>
                    <p>El broker es el encargado de supervisar las operaciones, de reportar los resultados a los inversores y de repartir los beneficios de forma proporcional entre los inversores según el capital que hayan depositado.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-us-countdown-area section-padding-100-0">
    <div class="container">
      <div class="row">
          <!-- Heading -->
          <div class="col-12">
              <div class="section-heading-3 text-center wow fadeInUp" data-wow-delay="300ms">
                  <h4 style="color:white">Ejemplo del servicio IT Manager</h4>
              </div>
          </div>
      </div>
        <div class="row">
<!-- Post Author Area -->
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 active text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/billeteras.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5>1.	¿Qué beneficios reciben el gestor y el inversor al final del intervalo comercial (1 mes)?</h5>
                    <p>Dado que, en el ejemplo, la cuenta IT Manager mostró un rendimiento del 30%, el gestor recibe el 30% de sus fondos iniciales, es decir, 375 USD. El inversor recibe el 30% de 500 USD, o sea, 150 USD.</p>
                </div>
            </div>
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/signo.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5>⦁	¿Cómo se distribuyen los beneficios y pérdidas en las cuentas IT Manager?</h5>
                    <p>Todos los beneficios y pérdidas en las cuentas IT Manager se distribuyen estrictamente en proporción a los fondos invertidos. En el ejemplo anterior, la parte del gestor es de  60%, 1250 USD, y la del inversor, de 40%, 500 USD; en total, 1750 USD.</p>
                </div>
            </div>
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/gestor2.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5>⦁	¿Cuánto recibe el gestor de parte del inversor en calidad de remuneración?</h5>
                    <p>Hablando de remuneración el inversor es el que paga al gestor el 15% de sus ganancias, según el ejemplo, 150 USD; en este caso, 22.5 USD. Esta remuneración es descontada por el Broker directamente de la cuenta del cliente.</p>
                </div>
            </div>
            <div style="margin-top: -1rem!important;" class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp">
                <!-- Avatar -->
                <div class="author-avatar">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/billetes.png" alt="">
                </div>
                <!-- Author Content -->
                <div class="author-content">
                    <h5>⦁	¿Cuál es el estado final de las cuentas del inversor y el gestor al finalizar el intervalo comercial y el pago de la remuneración?</h5>
                    <p>En este ejemplo, después de culminar el intervalo comercial y el pago de la remuneración, el inversor obtendrá: 500 + 150 – 22.5 = 627.5 USD. El gestor, por su parte, tendrá 1250 + 375 + 22.5 = 1647.5 USD.</p>
                </div>
            </div>
          </div>
      </div>
</section>
<section style="background:white !important;" id="Sectionpagos" class="about-us-countdown-area section-padding-100-0">
<div class="container">
      <div class="row">
   <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <!-- Icon -->
                    <div class="offer-icon">
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/logoPaypal.png" alt="">
                    </div>
                    <h5>Paga con Paypal</h5>                    
					<div class="col-12">
                        <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                            <a  class="btn confer-btn-white" href="<?= site_url('C_registro/Itmanager'); ?>" >Pagar <i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>

    <div class="col-12 col-md-s6 col-xl-6">
        <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
            <!-- Icon -->
            <div class="offer-icon">
                <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/Otropago.png" alt="">
            </div>
            <h5>Pagos por BTC</h5>                    
            <div class="col-12">
                <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                    <a class="btn confer-btn-white" href="<?= site_url('C_registro/BtcItmanager'); ?>"  >Pagar <i class="zmdi zmdi-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>    
        </div>
      </div>    
</section>
