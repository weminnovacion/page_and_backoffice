 <script>
    var baseurl = "<?php echo base_url(); ?>"
</script>
<style media="screen">
p{
  text-align: justify;
}
</style>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>
<!-- Breadcrumb Area Start -->
<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/contactoimg.jpg);">
   <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">Contacto</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Inicio</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contacto</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->

<!-- Contact Us Area Start -->
<section class="contact--us-area section-padding-100-0">
    <div class="container">
        <div class="row">
            <!-- Contact Us Thumb -->
            <div class="col-12 col-lg-6">
                <div class="contact-us-thumb mb-100">
                    <img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/44.jpg" alt="">
                </div>
            </div>

            <!-- Contact Form -->
            <div class="col-12 col-lg-6">
                <div class="contact_from_area mb-100 clearfix">
                    <!-- Contact Heading -->
                    <div class="contact-heading">
                        <h4>Contactenos</h4>
                        <p>Para solicitar servicios, aclarar dudas, brindarle soporte y toda la atencion que requiera, estamos para servirle.</p>
                    </div>
                    <div class="contact_form">
                        <form id="myform" name="myform" method="post">
                            <div class="contact_input_area">
                                <div class="row">
                                    <!-- Form Group -->
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control mb-30" name="name" id="name" placeholder="Nombre" required>
                                        </div>
                                    </div>
                                    <!-- Form Group -->
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control mb-30" name="apellido" id="apellido" placeholder="Apellido" required>
                                        </div>
                                    </div>
                                    <!-- Form Group -->
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control mb-30" name="email" id="email" placeholder="E-mail" required>
                                        </div>
                                    </div>
                                    <!-- Form Group -->
                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control mb-30" name="telefono" id="telefono" placeholder="Numero de telefono">
                                        </div>
                                    </div>
                                    <!-- Form Group -->
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea name="mensaje" class="form-control mb-30" id="mensaje" cols="30" rows="6" placeholder="Mensaje" required></textarea>
                                        </div>
                                    </div>
                                    <!-- Button -->
                                    <div class="col-12">
                                        <button class="btn confer-btn" id="btnsendmsg">Enviar Mensaje <i class="zmdi zmdi-long-arrow-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Us Area End -->


<!-- Contact Info Area -->
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="contact--info-area bg-boxshadow">
                <div class="row">
                    <!-- Single Contact Info -->
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="single-contact--info text-center">
                            <!-- Contact Info Icon -->
                            <div class="contact--info-icon">
                                <img src="<?php echo base_url();?>assets/plantillaInfo/img/core-img/icon-5.png" alt="">
                            </div>
                            <h5>Barranquilla Atlantico</h5>
                        </div>
                    </div>

                    <!-- Single Contact Info -->
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="single-contact--info text-center">
                            <!-- Contact Info Icon -->
                            <div class="contact--info-icon">
                                <img src="<?php echo base_url();?>assets/plantillaInfo/img/core-img/icon-6.png" alt="">
                            </div>
                            <h5>(+ 57) 3016125435</h5>
                        </div>
                    </div>

                    <!-- Single Contact Info -->
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="single-contact--info text-center">
                            <!-- Contact Info Icon -->
                            <div class="contact--info-icon">
                                <img src="<?php echo base_url();?>assets/plantillaInfo/img/core-img/icon-7.png" alt="">
                            </div>
                            <h5>contacto@investradecompany.com</h5>
                        </div>
                    </div>

                    <!-- Single Contact Info -->
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="single-contact--info text-center">
                            <!-- Contact Info Icon -->
                            <div class="contact--info-icon">
                                <img src="<?php echo base_url();?>assets/plantillaInfo/img/core-img/icon-8.png" alt="">
                            </div>
                            <h5>www.investradecompany.com</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/plantillaInfo/js/Datos/Contactos/contact.js"></script>
