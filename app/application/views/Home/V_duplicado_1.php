</section>
<!-- About Us And Countdown Area Start -->
<section class="about-us-countdown-area2 section-padding-100-0" id="about2">
    <div class="container">
        <div class="row align-items-center">
            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80">
                    <h3 class="wow fadeInUp" data-wow-delay="300ms">¡Libertad para crecer!</h3>
                    <p style="text-align: justify;" class="wow fadeInUp" data-wow-delay="300ms">La eduación es el puente que nos conduce desde donde estamos hasta donde queremos llegar, ITC nos proporciona el lugar para que esta transición se lleve a cabo, con las herramientas necesarias para alcanzar cualquier objetivo. </p>
                    <p style="text-align: justify;" class="wow fadeInUp" data-wow-delay="300ms">ITC cuenta con aliados estratégicos que tienen toda la experiencia y un conjunto de características poderosasas que nos permitirán vivir esta aventura de una manera más gratificante. </p>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                    <img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/educacion1.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>