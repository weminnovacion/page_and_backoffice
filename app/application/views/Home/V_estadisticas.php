<style>
section#clientearea {
    background-color: #ffffff  !important;
}
#section-counter:after {
    background: #ffffff  !important;
}
.ftco-counter .text strong.number {
    color: #0a3e7d !important;
}
span.subheading {
    color: #0a3e7d !important;
}
.ftco-counter .text span {
    display: block;
    font-size: 16px;
    color: #3971bd;
}
</style>
<!-- style="background-image: url(<?php echo base_url();?>assets/estadisticas/fondo.jpg);"  -->
<section class="ftco-section ftco-counter" id="section-counter" data-stellar-background-ratio="0.5">
<div class="container">
    <div class="row d-md-flex align-items-center">
        <div class="col-lg-4">
            <div class="heading-section pl-md-5 heading-section-white">
        <div class="ftco-animate">
            <span class="subheading">Algunos</span>
            <h2 class="mb-4">Datos interesantes</h2>
        </div>
        </div>
        </div>
        <div class="col-lg-8">
            <div class="row d-md-flex align-items-center">
            <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
                <div class="text">
                <strong class="number" data-number="5">0</strong>
                <span>Servicios</span>
                
                </div>
            </div>
            </div>
            <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
                <div class="text">
                <strong class="number" data-number="2">0</strong>
                <span>Productos</span>
                </div>
            </div>
            </div>
            <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
                <div class="text">
                <strong class="number" data-number="5000">0</strong>
                <span>Clientes capitalizados</span>
                
                </div>
            </div>
            </div>
            <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
                <div class="text">
                <strong class="number" data-number="153">0</strong>
                <span>Clientes felices</span>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
