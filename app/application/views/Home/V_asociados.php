<style>
h4#h4color {
    color: #ffffff;
}
p#pcolor {
    color: white;
}
</style>
<section class="our-sponsor-client-area  section-padding-100">
    <div class="container">
        <div class="row">
            <!-- Heading -->
            <div class="col-12">
                <div class="section-heading-2 text-center wow fadeInUp" data-wow-delay="300ms">
                    <h4 id="h4color">NUESTROS ASOCIADOS</h4>
                    <p id="pcolor">Partners &amp; Sponsors</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <!-- Our Sponsor area -->
                <div class="our-sponsor-area d-flex flex-wrap">
                    <!-- Single Sponsor -->
                    <div class="single-sponsor wow fadeInUp" data-wow-delay="300ms">
                        <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/plantillaInfo/img/core-img/cmc.png" alt=""></a>
                    </div>
                    <!-- Single Sponsor -->
                    <div class="single-sponsor wow fadeInUp" data-wow-delay="300ms">
                        <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/plantillaInfo/img/core-img/UP2.png" alt=""></a>
                    </div>
                    <!-- Single Sponsor -->
                    <div class="single-sponsor wow fadeInUp" data-wow-delay="300ms">
                        <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/plantillaInfo/img/core-img/binary-com.png" alt=""></a>
                    </div>
                    <!-- Single Sponsor -->
                    <div class="single-sponsor wow fadeInUp" data-wow-delay="300ms">
                        <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/plantillaInfo/img/core-img/HotForex-broker.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
