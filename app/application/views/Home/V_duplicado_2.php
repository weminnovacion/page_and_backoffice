<section class="about-us-countdown-area section-padding-100-0" id="about">
    <div class="container2">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                     <img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/investments.png" alt="">
                </div>
            </div>

            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80">
                    <h3 class="wow fadeInUp" data-wow-delay="300ms">Generadores de Ganancias</h3>
                    <p style="text-align: justify;" class="wow fadeInUp" data-wow-delay="100ms">Como organización impulsada por los resultados, buscamos constantemente personas ambiciosas y visionarias con el impulso y la flexibilidad necesaria para llevar su capital a un crecimiento exponencial con cualquiera de nuestros servicios que mejor se ajusten a su perfil de inversor y capital. </p>
                    <p><i class="zmdi zmdi-chart"></i> Forex</p>
                    <p><i class="zmdi zmdi-shield-check"></i> Opciones Binarias</p>
                    <p><i class="zmdi zmdi-globe-alt"></i> Arbitraje de Divisas</p>
                    <p><i class="zmdi zmdi-money"></i> Criptomonedas</p>
                    <a href="#" id="masInfo" class="btn confer-btn-white">Ver Servicios <i class="zmdi zmdi-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>