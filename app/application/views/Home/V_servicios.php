<style media="screen">
.breadcrumb-area {
  position: relative !important;
  z-index: 1 !important;
  height: 600px !important;
}

.single-ticket-pricing-table .ticket-pricing-table-details p {
    color: #5d5e8d;
}

#section_service .confer-btn-white {
    border: 1px solid;
}

</style>
 <script>
    var baseurl = "<?php echo base_url(); ?>"
</script>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>

<!-- Our Ticket Pricing Table Area Start -->
<section id="section_service" class="our-ticket-pricing-table-area section-padding-100-0">
    <div class="container">
        <div class="row">
            <!-- Heading -->
            <div class="col-12">
                <div class="section-heading-3 text-center wow fadeInUp" data-wow-delay="300ms">
                    <h4>NUESTROS SERVICIOS</h4>
                    <p>Trading Automático</p>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- FX TRADE -->
            <div class="col-12 col-md-7 col-lg-4">
                <div class="single-ticket-pricing-table style-2 active text-center mb-100 wow fadeInUp" data-wow-delay="300ms" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/FondoServicio.png); background-size:cover;">
                    <h6 class="ticket-plan">FX TRADE</h6>
                    <!-- Ticket Icon -->
                    <div class="ticket-icon">
                        <!-- <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/plan2.png" alt=""> -->
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/Logo_FxTrade.png" alt="">
                    </div>
                  <!--  <h2 class="ticket-price"><span>$</span>99</h2>-->
                  <br>
                    <div class="ticket-pricing-table-details">
                        <p><i class="zmdi zmdi-check"></i> Trading Automático - Forex</p>
                        <p><i class="zmdi zmdi-check"></i> FxTrade BOT</p>
                        <p><i class="zmdi zmdi-check"></i> Fenix2.0 BOT</p>
                        <p><i class="zmdi zmdi-check"></i> Rentabilidad Variable: 20% a 50% mensual</p>
                        <p><i class="zmdi zmdi-check"></i> Bróker autorizado y regulado: HOTFOREX</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Cuenta Individual</p> -->
                        <!-- <p><i class="zmdi zmdi-check"></i> Soporte y Monitoreo de cuenta</p> -->
                        <!-- <p><i class="zmdi zmdi-check"></i> Acceso al programa de comisiones</p> -->
                        <p><i class="zmdi zmdi-check"></i> Capital mínimo: $1.000 USD</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Suscripción mensual</p> -->
                    </div>
                    <div class="col-12">
                        <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                            <a class="btn confer-btn-white" href="<?= site_url('C_servicios/serviciosFxtrade'); ?>">Saber mas <i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- IT MANAGER -->
            <div class="col-12 col-md-7 col-lg-4">
                <div class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp" data-wow-delay="300ms" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/FondoServicio.png); background-size:cover;">
                    <h6 class="ticket-plan">IT MANAGER</h6>
                    <!-- Ticket Icon -->
                    <div class="ticket-icon">
                        <!-- <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/plan1.png" alt=""> -->
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/Logo_ItManager.png" alt="">
                    </div>
                <!--  <h2 class="ticket-price"><span>$</span>59</h2> -->
                <br>
                    <div class="ticket-pricing-table-details">
                        <p><i class="zmdi zmdi-check"></i> Cuenta Administrada - Forex</p>
                        <p><i class="zmdi zmdi-check"></i> Gestión de fondos por Traders Profesionales</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Operativas: IT Fx y IT Mc</p> -->
                        <p><i class="zmdi zmdi-check"></i> Rentabilidad Variable: 15% a 20% mensual</p>
                        <p><i class="zmdi zmdi-check"></i> Bróker autorizado y regulado: HOTFOREX</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Cuenta Individual</p> -->
                        <p><i class="zmdi zmdi-check"></i> Soporte y monitoreo de cuenta</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Acceso al programa de comisiones</p> -->
                        <p><i class="zmdi zmdi-check"></i> Capital mínimo: $3.000 USD</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Suscripción mensual</p> -->

                    </div>
                    <div class="col-12">
                        <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                            <a class="btn confer-btn-white" href="<?= site_url('C_servicios/serviciosPamm'); ?>">Saber mas <i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FX BINARY -->
            <div class="col-12 col-md-7 col-lg-4">
                <div class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp" data-wow-delay="300ms" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/FondoServicio.png); background-size:cover;">
                    <h6 class="ticket-plan">IT BINARY</h6>
                    <!-- Ticket Icon -->
                    <div class="ticket-icon">
                        <!-- <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/plan3.png" alt=""> -->
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/LogoIt_Binary.png" alt="">
                    </div>
                  <!--  <h2 class="ticket-price"><span>$</span>199</h2> -->
                  <br>
                    <div class="ticket-pricing-table-details">
                        <p><i class="zmdi zmdi-check"></i> Trading Semi Automático – Opciones Binarias</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> ITBinary BOT</p> -->
                        <p><i class="zmdi zmdi-check"></i> Operativa: Dígitos</p>
                        <p><i class="zmdi zmdi-check"></i> Rentabilidad Variable: 1% a 5% diario</p>
                        <p><i class="zmdi zmdi-check"></i> Bróker autorizado y regulado: BINARY.COM</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Cuenta Individual</p> -->
                        <!-- <p><i class="zmdi zmdi-check"></i> Acceso al programa de comisiones</p> -->
                        <p><i class="zmdi zmdi-check"></i> Capital mínimo: $1.000 USD</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Suscripción mensual</p> -->
                       
                    </div>
                    <div class="col-12">
                        <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                            <a class="btn confer-btn-white" href="<?= site_url('C_servicios/serviciosItbinary'); ?>">Saber mas <i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2"></div>
            <!-- CAB CAPITAL -->
            <div class="col-12 col-md-7 col-lg-4">
                <div class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp" data-wow-delay="300ms" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/FondoServicio.png); background-size:cover;">
                    <h6 class="ticket-plan">CAB CAPITAL</h6>
                    <!-- Ticket Icon -->
                    <div class="ticket-icon">
                        <!-- <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/plan1.png" alt=""> -->
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/plan1.png" alt="">
                    </div>
                <!--  <h2 class="ticket-price"><span>$</span>59</h2> -->
                <br>
                    <div class="ticket-pricing-table-details">
                        <!-- <p><i class="zmdi zmdi-check"></i> Gestión de Capital</p> -->
                        <p><i class="zmdi zmdi-check"></i> Arbitraje manual de divisas.</p>
                        <p><i class="zmdi zmdi-check"></i> Rentabilidad Fija del 4%</p>
                        <p><i class="zmdi zmdi-check"></i> Contrato legal y notariado</p>
                        <p><i class="zmdi zmdi-check"></i> Contrato mínimo de 4 Meses</p>
                        <p><i class="zmdi zmdi-check"></i> Pagos semanales, quincenales o mensuales</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Acceso al programa de comisiones</p> -->
                        <p><i class="zmdi zmdi-check"></i> Gestión de capital del 15% sobre ganancias</p>
                        <p><i class="zmdi zmdi-check"></i> Capital mínimo: $7.000 USD</p>
                    </div>
                    <div class="col-12">
                        <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                            <a class="btn confer-btn-white" href="<?= site_url('C_servicios/serviciosPamm'); ?>">Saber mas <i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- MONEY PRO -->
            <div class="col-12 col-md-7 col-lg-4">
                <div class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp" data-wow-delay="300ms" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/FondoServicio.png); background-size:cover;">
                    <h6 class="ticket-plan">MONEY PRO</h6>
                    <!-- Ticket Icon -->
                    <div class="ticket-icon">
                        <!-- <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/plan1.png" alt=""> -->
                        <img src="<?php echo base_url();?>/assets/plantillaInfo/img/core-img/LogoMoneyPro.png" alt="">
                    </div>
                <!--  <h2 class="ticket-price"><span>$</span>59</h2> -->
                <br>
                    <div class="ticket-pricing-table-details">
                        <p><i class="zmdi zmdi-check"></i> Cuenta común administrada – Opciones Binarias</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> ITBinary BOT</p> -->
                        <!-- <p><i class="zmdi zmdi-check"></i> Operaciones manuales por Traders</p> -->
                        <p><i class="zmdi zmdi-check"></i> Rentabilidad Fija del 15%</p>
                        <p><i class="zmdi zmdi-check"></i> Bróker autorizado y regulado: BINARY.COM</p>
                        <!-- <p><i class="zmdi zmdi-check"></i> Acceso al programa de comisiones</p> -->
                        <p><i class="zmdi zmdi-check"></i> Capital: $100 USD a $900 USD</p>
                        <p><i class="zmdi zmdi-check"></i> Suscripción del servicio: $25 USD Pago único.</p>
                    </div>
                    <div class="col-12">
                        <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                            <a class="btn confer-btn-white" href="<?= site_url('C_servicios/serviciosPamm'); ?>">Saber mas <i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
