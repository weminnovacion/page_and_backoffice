 <script>
    var baseurl = "<?php echo base_url(); ?>"
</script>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>
<style>

h3.wow.fadeInUp {
    font-size: 30px !important;
}

h6.wow.fadeInUp {
  color: #ffffff !important;
}
.container2 {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    text-align: left;
}

.single-welcome-slide .welcome-text {
    position: relative;
    z-index: 1;
    top: -26px !important;
}

.single-welcome-slide {
    position: relative;
    z-index: 2;
    width: 100%;
    height: 849px !important;
    background-color: #3971bd;
}

iframe {
   max-width: 100%;
    max-height: 100%;
}
</style>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js" type="text/javascript"></script>
<script>
    var baseurl = "<?php echo base_url(); ?>"
</script>
<!-- Welcome Area Start -->
<div id="home">
<section class="welcome-area">
    <div class="welcome-slides owl-carousel">
        <!-- Single Slide -->
        <div class="single-welcome-slide bg-img bg-overlay jarallax" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/1.jpg);">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <!-- Welcome Text -->
                    <div class="col-12">
                        <div class="welcome-text text-right">
                            <h2 data-animation="fadeInUp" data-delay="300ms">El viaje al éxito<br> Inicia con solo un paso</h2>
                            <h6 data-animation="fadeInUp" data-delay="500ms">Educación Financiera, Coaching, Trading, Servicios Automáticos</h6>
                            <div class="hero-btn-group" data-animation="fadeInUp" data-delay="700ms">
                                <a href="#" id="masInfo" class="btn confer-btn">Mas Información <i class="zmdi zmdi-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Single Slide -->
        <div class="single-welcome-slide bg-img bg-overlay jarallax" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/45.jpg);">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <!-- Welcome Text -->
                    <div class="col-12">
                        <div class="welcome-text-two text-center">
                            <h5 data-animation="fadeInUp" data-delay="100ms">Aprende a invertir tu dinero</h5>
                            <h2 data-animation="fadeInUp" data-delay="300ms">Trading Automático</h2>
                            <!-- Event Meta -->
                            <div class="hero-btn-group" data-animation="fadeInUp" data-delay="700ms">
                                <a href="<?php echo base_url();?>" class="btn confer-btn m-2">Saber más <i class="zmdi zmdi-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="single-welcome-slide bg-img bg-overlay jarallax" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/Educacion.jpg);">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <!-- Welcome Text -->
                    <div class="col-12">
                        <div class="welcome-text-two text-center">
                          <h5 data-animation="fadeInUp" data-delay="100ms">Nos preocupamos por tu aprendizaje</h5>
                          <h2 data-animation="fadeInUp" data-delay="300ms">Educación</h2>
                            <!-- Event Meta -->
                            <div class="hero-btn-group" data-animation="fadeInUp" data-delay="700ms">
                              <a href="<?php echo base_url();?>" class="btn confer-btn m-2">Saber más <i class="zmdi zmdi-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-welcome-slide bg-img bg-overlay jarallax" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/Couching.jpg);">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <!-- Welcome Text -->
                    <div class="col-12">
                        <div class="welcome-text-two text-center">
                            <h5 data-animation="fadeInUp" data-delay="100ms">Te ayudamos a alcanzar tus metas</h5>
                            <h2 data-animation="fadeInUp" data-delay="300ms">Coaching</h2>
                            <!-- Event Meta -->
                            <div class="hero-btn-group" data-animation="fadeInUp" data-delay="700ms">
                              <a href="<?= site_url('C_servicios/educacion/#abouta'); ?>" class="btn confer-btn m-2">Saber más <i class="zmdi zmdi-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="icon-scroll" id="scrollDown"></div>
</section>
<!-- About Us And Countdown Area Start -->
<section class="about-us-countdown-area2 section-padding-100-0" id="about2">
    <div class="container">
        <div class="row align-items-center">
            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80">
                    <h3 class="wow fadeInUp" data-wow-delay="300ms">¡Bienvenidos!</h3>
                    <p style="text-align: justify;" class="wow fadeInUp" data-wow-delay="300ms">En ITC, ofrecemos más que solo la experiencia y las herramientas para obtener éxito financiero en su vida. Ofrecemos la capacidad de crear vidas más satisfactorias, llenas de lo que en verdad importa, ricas en experiencias significativas, diseñadas para crear valor en los demás. </p>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                    <img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/logo.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-us-countdown-area section-padding-100-0" id="about" style="background-image:url('<?php echo base_url();?>assets/plantillaInfo/img/bg-img/Fondo.png')">
    <div class="container2">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                     <img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/Imagen.png" alt="" >
                </div>
            </div>

            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80">
                    <h3 class="wow fadeInUp" data-wow-delay="300ms">Su camino a la Libertad Financiera</h3>
                    <p style="text-align: justify;" class="wow fadeInUp" data-wow-delay="100ms">Nos dedicamos a ayudar a que su dinero trabaje para usted. Nuestros servicios están enfocados en hacer crecer su capital, utilizando estrategias y herramientas con la mayor tecnología, logrando un éxito sostenible. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo base_url(); ?>assets/plantillaInfo/js/Datos/Inicio/home.js"></script>
