<div style=" background-image:url('<?php echo base_url();?>assets/plantillaInfo/img/bg-img/Fondo.png');" class="our-blog-area section-padding-100-0">
    <div class="container">
        <div class="row">
          <div class="col-12">
              <div class="section-heading-2 text-center wow fadeInUp" data-wow-delay="300ms">
                  
                  <h4>Productos</h4>
              </div>
          </div>
                <div class="col-12 col-md-6">
                    <div style="background-color:#3971bc" class="single-blog-area style-2 wow fadeInUp" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin1.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">DISTRIBUTOR FEE</a>
                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i> Licencia Anual</a>
                                <a class="post-author" href="#"><i class="zmdi zmdi-account"></i> Basico</a>
                            </div>
                            <h2 class="ticket-price"><span>$</span>25 USD</h2>
                            <div class="ticket-pricing-table-details">
                                <p><i class="zmdi zmdi-check"></i>Acceso al programa de comisiones</p>
                                <p><i class="zmdi zmdi-check"></i>Bono de venta directa: 20% Sobre el paquete adquirido por los clientes</p>
                                <p><i class="zmdi zmdi-check"></i>Bono Residual: 10% Mensual Sobre rentabilidad de los referidos directos</p><br>
                                <br>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <!-- Single Blog Area -->
                <div class="col-12 col-md-6">
                    <div class="single-blog-area style-2 wow fadeInUp" style="background-color:#3971bc" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin2.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">ITBINARY BOT</a>
                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i>Licencia de por vida</a>
                                <a class="post-author" href="#"><i class="zmdi zmdi-account"></i>Avanzado</a>
                            </div>
                            <h2 class="ticket-price"><span>$</span>2.000 USD</h2>
                            <div class="ticket-pricing-table-details">
                                <p><i class="zmdi zmdi-check"></i> Estrategia Automatizada</p>
                                <p><i class="zmdi zmdi-check"></i> Operativa basada en índices volátiles</p>
                                <p><i class="zmdi zmdi-check"></i> Integra Bot de Telegram para seguimiento de operaciones</p>
                                <p><i class="zmdi zmdi-check"></i> Operación en Binary.com</p>
                                <p><i class="zmdi zmdi-check"></i> De lunes a domingos</p>
                                <p><i class="zmdi zmdi-check"></i> Soporte y actualizaciones</p>
                                <p><i class="zmdi zmdi-check"></i> Acceso al programa de comisiones</p>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Single Blog Area -->
                <!-- <div class="col-12 col-md-6 col-xl-4">
                    <div class="single-blog-area style-2 wow fadeInUp" style="background-color:#3971bc" data-wow-delay="300ms">

                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin3.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">½ Pin Director País</a>

                            <div class="post-meta">
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i>Duracion 40 Meses</a>
                                <a class="post-author" href="#"><i class="zmdi zmdi-account"></i>Avanzado</a>
                            </div>
                            <h2 class="ticket-price"><span>$</span>750 USD</h2>
                            <div class="ticket-pricing-table-details">
                                <p><i class="zmdi zmdi-check"></i> 5 Certificaciones</p>
                                <p><i class="zmdi zmdi-check"></i> 20 Profundizaciones</p>
                                <p><i class="zmdi zmdi-check"></i> Acceso a la Universidad del Coach</p>
                                <p><i class="zmdi zmdi-check"></i> Programa de Comisiones 60%</p>
                                <p><i class="zmdi zmdi-check"></i> Regalías sobre el 50% de los ingresos anuales de la CMC</p>
                                <p><i class="zmdi zmdi-check"></i> Acceso a una de las Convenciones Mundiales    (Santa Marta o Punta Cana)</p>
                            </div>
                        </div>
                    </div>
                </div> -->
              </div>
          </div>
      </div>
