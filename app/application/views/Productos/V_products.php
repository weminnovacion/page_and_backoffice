<style media="screen">
.breadcrumb-area {
  position: relative !important;
  z-index: 1 !important;
  height: 600px !important;
}
.single-ticket-pricing-table.style-2 {
    background-color: #ffffff !important;
    box-shadow: 0px 8px 27px 0px rgba(57, 113, 188, 0.71) !important;
}
</style>
<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/contactoimg.jpg);">
   <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">Nuestros productos</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Inicio</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Nuestros productos</li>
                            <input id="txtTipoServicios1" name="txtTipoServicios1" value = "<?php echo $TipoServicio;?>"  type="hidden">
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container section-padding-100-0">
    <div id="product" class="row">
    
    </div>
</div>

<script>
    var baseurl = "<?php echo base_url(); ?>"
</script>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/Datos/Servicios/Productos.js"></script>
