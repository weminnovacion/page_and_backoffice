<!-- Bootstrap Stylesheets -->
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/PlantillaRegistro/css/bootstrap.min.css"> -->
<!-- Font Awesome Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/PlantillaRegistro/css/font-awesome.min.css">
<!-- Template Main Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/PlantillaRegistro/css/contact-form.css" type="text/css">	
<style>
.alert{
    width: 89% !important ;
}
.btn {
    font-size: 16px;
    overflow: hidden;
	padding: 0px 18px;
    text-transform: uppercase;
}
section#contact-form-section {
    background: #3971bd;
}
.item-wrap {
    background: white;
	border-radius: 25px;
}
.popup-form .form-group .input-group-icon{
background: #3971bd !important;
padding: 10px 7px 7px !important;
border-radius: 20px 0px 0px 20px;
}
.item-title {
    color: #ffffff;
	margin: 30px auto 0px;
}
.popup-form .form-control {
    border-radius: 25px;
}
.popup-form .btn.btn-custom {
    height: 40px;
	width: 150px;
    border-radius: 20px;
    background: #3971bd;
}
.form-group.last.col-sm-12.center {
    text-align: center;
}
.section-padding-100-0 {
    padding-top: 25px;
}
</style>

<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/contactoimg.jpg);">
   <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">REGISTRATE</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Inicio</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $Registro;?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="contact-form-section" class="form-content-wrap">
    <div class="container">
        <div class="row">
            <div class="tab-content">
			<h2 class="item-title text-center">Registrate y adquiere tu plan!</h2>
                <div class="col-sm-12">
                    <div class="item-wrap">
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="item-content colBottomMargin">
                                <div class="item-info">
                                </div><!--End item-info -->
                            </div><!--End item-content -->
                            </div><!--End col -->
                        <div class="col-md-12">
                            <form class="popup-form">
                                <div class="row">                                   
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="name1" id="txtname1" placeholder="Primer Nombre*" class="form-control" type="text"> 
                                        <div class="input-group-icon"><i class="fa fa-user"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="apellido1" id="txtapellido1" placeholder="Primer Apellido*" class="form-control" type="text"> 
                                        <div class="input-group-icon"><i class="fa fa-user"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="apellido2" id="txtapellido2" placeholder="Segundo Apellido (Opcional)*" class="form-control" type="text"> 
                                        <div class="input-group-icon"><i class="fa fa-user"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="ident" id="txtident" placeholder="Tu Identificación*" class="form-control" type="number"> 
                                        <div class="input-group-icon"><i class="fa fa-user"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="email" id="txtemail" placeholder="Tu E-mail*" class="form-control" type="email">
                                        <div class="input-group-icon"><i class="fa fa-envelope"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="telefono" id="txttelefono" placeholder="Tu Teléfono*" class="form-control" type="number">
                                        <div class="input-group-icon"><i class="fa fa-phone"></i></div> 
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="pass" id="txtpass" placeholder="Crea tu contraseña*" class="form-control" type="password">
                                        <div class="input-group-icon"><i class="fa fa-book"></i></div> 
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="passw" id="txtpassw" placeholder="Repite tu contraseña*" class="form-control" type="password">
                                        <div class="input-group-icon"><i class="fa fa-book"></i></div> 
                                    </div><!-- end form-group -->      
                                    <div class="form-group last col-sm-12">
                                    <input id="TipoServicio" name ="TipoServicio" value ="<?php echo $TipoServicio;?>"  type="hidden">
                                    </div><!-- end form-group -->                            
                                    <div class="form-group last col-sm-12 center">
                                        <button id="registroId" type="button" onclick="Registrar()" class="btn btn-custom">
                                         <i class='fa fa-envelope'></i> Enviar
                                        </button>
                                    </div><!-- end form-group -->	
                                    <div class="clearfix"></div>
                                </div><!-- end row -->
                                </form><!-- end form -->
                            </div>
                        </div><!--End row -->
                    </div><!-- end item-wrap -->
                </div><!--End col -->
            </div><!--End tab-content -->
        </div><!--End row -->
    </div><!--End container -->
</section>		
<div class="container section-padding-100-0">
    <div id="product" class="row">
    
    </div>
</div>
<script>
var baseurl = "<?php echo base_url(); ?>"
</script>

<script src="<?php echo base_url(); ?>assets/PlantillaRegistro/js/Datos/Registro/RegistroStatico.js"></script>


