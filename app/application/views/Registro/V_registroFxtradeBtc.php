<!-- Bootstrap Stylesheets -->
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/PlantillaRegistro/css/bootstrap.min.css"> -->
<!-- Font Awesome Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/PlantillaRegistro/css/font-awesome.min.css">
<!-- Template Main Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/PlantillaRegistro/css/contact-form.css" type="text/css">	
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<style>
.alert{
    width: 89% !important ;
}
.btn {
    font-size: 16px;
    overflow: hidden;
	padding: 0px 18px;
    text-transform: uppercase;
}
p.tagets {
    color: #434848 !important;
}
h5.tagets {
    color: #0a3e7d !important;
}
section#contact-form-section {
    background: #3971bd;
}
.item-wrap {
    background: white;
	border-radius: 25px;
}
.popup-form .form-group .input-group-icon{
background: #3971bd !important;
padding: 10px 7px 7px !important;
border-radius: 20px 0px 0px 20px;
}
img#btc {
    width: 20%;
}
.item-title {
    color: #ffffff;
	margin: 30px auto 0px;
}
.popup-form .form-control {
    border-radius: 25px;
}
.popup-form .btn.btn-custom {
    height: 40px;
	width: 150px;
    border-radius: 20px;
    background: #3971bd;
}
.form-group.last.col-sm-12.center {
    text-align: center;
}
.section-padding-100-0 {
    padding-top: 25px;
}
.filter-option-inner-inner {
    background: #3971bd;
    text-align: center;
    border-radius: 5%;
    color: white;
}
.btn-light {
    color: #212529 !important;
    background-color: #3971bd !important;
    border-color: #3971bd !important;
}
.btn-light:not(:disabled):not(.disabled).active, .btn-light:not(:disabled):not(.disabled):active, .show>.btn-light.dropdown-toggle {
    color: #212529 !important;
    background-color: #3971bd !important;
    border-color: #3971bd !important;
}
</style>

<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(<?php echo base_url();?>assets/plantillaInfo/img/bg-img/contactoimg.jpg);">
   <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">REGISTRATE</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Inicio</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $Registro;?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="contact-form-section" class="form-content-wrap">
    <div class="container">
        <div class="row">
            <div class="tab-content">
			<h2 class="item-title text-center">Registrate y adquiere tu plan!</h2>
                <div class="col-sm-12">
                    <div class="item-wrap">
                      <div class="row">
                        <div class="col-sm-12">
                            <div class="item-content colBottomMargin">
                                <div class="item-info">
                                </div><!--End item-info -->
                            </div><!--End item-content -->
                            </div><!--End col -->
                        <div class="col-md-12">
                            <form class="popup-form">
                                <div class="row">                                   
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="name1" id="txtname1" placeholder="Primer Nombre*" class="form-control" type="text"> 
                                        <div class="input-group-icon"><i class="fa fa-user"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="apellido1" id="txtapellido1" placeholder="Primer Apellido*" class="form-control" type="text"> 
                                        <div class="input-group-icon"><i class="fa fa-user"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="apellido2" id="txtapellido2" placeholder="Segundo Apellido (Opcional)*" class="form-control" type="text"> 
                                        <div class="input-group-icon"><i class="fa fa-user"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="ident" id="txtident" placeholder="Tu Identificación*" class="form-control" type="number"> 
                                        <div class="input-group-icon"><i class="fa fa-user"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="email" id="txtemail" placeholder="Tu E-mail*" class="form-control" type="email">
                                        <div class="input-group-icon"><i class="fa fa-envelope"></i></div>
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="telefono" id="txttelefono" placeholder="Tu Teléfono*" class="form-control" type="number">
                                        <div class="input-group-icon"><i class="fa fa-phone"></i></div> 
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="pass" id="txtpass" placeholder="Crea tu contraseña*" class="form-control" type="password">
                                        <div class="input-group-icon"><i class="fa fa-book"></i></div> 
                                    </div><!-- end form-group -->
                                    <div class="form-group col-sm-6">
                                        <div class="help-block with-errors"></div>
                                        <input name="passw" id="txtpassw" placeholder="Repite tu contraseña*" class="form-control" type="password">
                                        <div class="input-group-icon"><i class="fa fa-book"></i></div> 
                                    </div><!-- end form-group -->      
                                    <div class="form-group last col-sm-12">
                                    <input id="TipoServicio" name ="TipoServicio" value ="<?php echo $TipoServicio;?>"  type="hidden">
                                    </div><!-- end form-group -->                            
                                    <div class="form-group last col-sm-12 center">
                                        <button id="registroId" type="button" onclick="Registrar()" class="btn btn-custom">
                                         <i class='fa fa-envelope'></i> Enviar
                                        </button>
                                    </div><!-- end form-group -->	
                                    <div class="clearfix"></div>
                                </div><!-- end row -->
                                </form><!-- end form -->
                            </div>
                        </div><!--End row -->
                    </div><!-- end item-wrap -->
                </div><!--End col -->
            </div><!--End tab-content -->
        </div><!--End row -->
    </div><!--End container -->
</section>		
<!-- <div class="container">
<div class="row">
    <div class="col-12 col-md-6 col-xl-6">
        <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
            <div class="offer">
                <img id="btc" src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/btc.png" alt="">
            </div>
            <select class="selectpicker">
                <option>BUSCADORES</option>
                <option>Ketchup</option>
                <option>Barbecue</option>
            </select>
            <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">                                         
                <a class="btn confer-btn-white" href="<?= site_url('C_registro/BtcFxtrade'); ?>"  >Pagar <i class="zmdi zmdi-long-arrow-right"></i></a>                            
            </div>
        </div>

        <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
            <div class="offer">
                <img id="btc" src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/btc.png" alt="">
            </div>
            <select class="selectpicker">
                <option>BUSCADORES</option>
                <option>Ketchup</option>
                <option>Barbecue</option>
            </select>
            <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">                                         
                <a class="btn confer-btn-white" href="<?= site_url('C_registro/BtcFxtrade'); ?>"  >Pagar <i class="zmdi zmdi-long-arrow-right"></i></a>                            
            </div>
        </div>
    </div>  
</div>
</div> -->
<section class="what-we-offer-area section-padding-100-70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading-3 text-center wow fadeInUp" data-wow-delay="300ms">
                    <h4>METODOS DE PAGO</h4>
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col-12 col-md-12">
             
           </div>
            <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <div class="offer">
                     <img id="btc" src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/btc.png" alt="">
                    </div><br>
                        <select id="select" class="selectpicker">
                            <option>BUSCADORES</option>
                            <option>1</option>
                            <option>2</option>
                        </select>
                    <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">                                         
                        <a class="btn confer-btn-white" href="<?= site_url('C_registro/BtcFxtrade'); ?>"  >Pagar <i class="zmdi zmdi-long-arrow-right"></i></a>                            
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-xl-6">
                <div class="single-we-offer-content text-center wow fadeInUp" data-wow-delay="0.3s">
                    <div class="offer">
                        <img id="btc" src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/skrill.png" alt="">
                    </div>
                    <h5 class="tagets">Realiza tus pagos por skrill de forma rapida y segura al siguiente correo:</h5>
                    <p class="tagets">payments@investradecompany.com</p>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
var baseurl = "<?php echo base_url(); ?>"
</script>

<script src="<?php echo base_url(); ?>assets/PlantillaRegistro/js/Datos/Registro/RegistroStatico.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>    