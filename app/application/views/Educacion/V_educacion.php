 <script>
    var baseurl = "<?php echo base_url(); ?>"
</script>
<style>
.breadcrumb-area {
  height: 660px !important;
}
</style>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>
<!-- Breadcrumb Area Start -->
<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/estudio.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">Educación</h2>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Educación</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->



<!-- About Us And Countdown Area Start -->
<section class="about-us-countdown-area section-padding-100-0" id="about1">
    <div class="container">
        <div class="row align-items-center">
            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80">
                    <h6 class="wow fadeInUp" style="color:#062a54 !important" data-wow-delay="300ms">EDUCACIÓN EN EL MERCADO DE DIVISAS Y CRIPTOMONEDAS</h6>
                    <h3 class="wow fadeInUp" style="color:#062a54" data-wow-delay="300ms">ACADEMIA UNIVERSAL PROFITS</h3>
                    <p class="wow fadeInUp" data-wow-delay="300ms"><a  style="font-size:17px;color:#062a54">Plataforma  E-Learning:</a> con más de 100 videos de trading que son fáciles de seguir y le dan la capacidad de aprender a su propio ritmo. </p>
                    <p class="wow fadeInUp" data-wow-delay="300ms"><a  style="font-size:17px;color:#062a54">Educación básica a avanzada en forex y criptomonedas:</a> desglosado en módulos paso a paso para que pueda aprender a dominar los mercados. </p>
                    <p class="wow fadeInUp" data-wow-delay="300ms"><a  style="font-size:17px;color:#062a54">Sesiones de Trading en Vivo:</a> conéctate y aprende a operar en vivo con traders  de alto impacto.
                    Desarrolla tus propias habilidades o simplemente sigue a los expertos.</p>
                    <div class="col-12">
                        <div class="more-speaker-btn mt-20 mb-40 wow fadeInUp" data-wow-delay="300ms">
                            <a class="btn confer-btn-white" href="https://universal-profits.com/mioficina/register?referred_id=2492" target="_blank">Visitar Universal Profits <i class="zmdi zmdi-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- About Thumb -->
            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/Up.png" alt="">
                </div>
            </div>
        </div>
    </div>

    <!-- Counter Up Area -->
    <div class="countdown-up-area">
        <div class="container">
            <div class="row align-items-center">
                <!-- Our client area -->
                <div class="col-12">
                    <div class="our-client-area mt-100 wow fadeInUp" data-wow-delay="300ms">
                        <!-- client Slider -->
                        <div class="client-area owl-carousel">
                            <!-- Single client Slider -->
                            <div class="single-client-content">
                                <!-- Single client Text -->
                                <div class="single-client-text">
                                    <p>"La mente es el activo mas poderoso que tenemos los seres humanos". Por ende invierte en tu mente y estaras haciendo el negocio mas grande de tu vida</p>
                                    <!-- Single client Thumb and info -->
                                    <div class="single-client-thumb-info d-flex align-items-center">
                                        <!-- Single client Thumb -->
                                        <div class="single-client-thumb">
                                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/rk.jpg" alt="">
                                        </div>
                                        <!-- Single client Info -->
                                        <div class="client-info">
                                            <h6>Robert Kiyosaki</h6>
                                            <p> Empresario, Inversor, Escritor...</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single client Icon -->
                                <div class="client-icon">
                                    <i class="zmdi zmdi-quote"></i>
                                </div>
                            </div>

                            <!-- Single client Slider -->
                            <div class="single-client-content">
                                <!-- Single client Text -->
                                <div class="single-client-text">
                                    <p>"Uno de los grandes errores que comete la gente, es tratar de forzar su interes. Tu no eliges tus pasiones; Tus pasiones te eligen a ti"</p>
                                    <!-- Single client Thumb and info -->
                                    <div class="single-client-thumb-info d-flex align-items-center">
                                        <!-- Single client Thumb -->
                                        <div class="single-client-thumb">
                                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/jb.jpg" alt="">
                                        </div>
                                        <!-- Single client Info -->
                                        <div class="client-info">
                                            <h6>Jeff Bezos</h6>
                                            <p>Empresario, CEO de Amazon</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single client Icon -->
                                <div class="client-icon">
                                    <i class="zmdi zmdi-quote"></i>
                                </div>
                            </div>

                            <!-- Single client Slider -->
                            <div class="single-client-content">
                                <!-- Single client Text -->
                                <div class="single-client-text">
                                    <p>"Lo que haces ahora es lo que dice como funcionara tu cuerpo y tu mente dentro de diez, veinte, treinta y cuarenta años.""</p>
                                    <!-- Single client Thumb and info -->
                                    <div class="single-client-thumb-info d-flex align-items-center">
                                        <!-- Single client Thumb -->
                                        <div class="single-client-thumb">
                                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/wb.jpg" alt="">
                                        </div>
                                        <!-- Single client Info -->
                                        <div class="client-info">
                                            <h6>Warren Buffett</h6>
                                            <p>Inversor, Empresario </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single client Icon -->
                                <div class="client-icon">
                                    <i class="zmdi zmdi-quote"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us And Countdown Area End -->

<!-- About Us And Countdown Area Start -->
<section class="about-us-countdown-area2 section-padding-100-0" id="about">
    <div class="container">
        <div class="row align-items-center">
            <!-- About Thumb -->
            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                    <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/CMC.png" alt="">
                </div>
            </div>
            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80">
                    <h6 class="" style="color:#0a3e7d" data-wow-delay="300ms"><u> EDUCACIÓN DEL SER</u></h6>
                    <h3 class="wow fadeInUp" data-wow-delay="300ms">LA MEJOR ESCUELA VIRTUAL DE COACHING</h3>
                    <p class="wow fadeInUp" data-wow-delay="300ms">Creadores de contextos emocionales donde la gente florece, aplicando coaching en la reprogramación sistémica y el desarrollo personal con procesos sostenibles, Interviniendo en los dominios del observador desde la mirada ontológica (lenguaje cuerpo y emoción), comprendiendo cada dominio.</p>
                    <a href="https://cmcoaches.com/" target="_blank" class="btn confer-btn mt-50 wow fadeInUp" data-wow-delay="300ms">Visitar CMC <i class="zmdi zmdi-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <section style="padding-top: initial;" class="confer-blog-details-area section-padding-100-0">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Post Details Area -->
                <div class="col-12 col-lg-8 col-xl-9">
                    <div class="pr-lg-4 mb-100">
                        <!-- Post Content -->
                        <div class="post-details-content">

                        <div class="row">
                        <div class="col-6">
                            <img class="mb-30" src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/coach1.jpg" alt="">
                        </div>
                        <div class="col-6">
                            <img class="mb-30" src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/coach2.jpg" alt="">
                        </div>
                        <div class="col-12 col-md-12">
                            <div class="about-content-text mb-80">
                        <h5>¿Qué es el Coaching? </h5>
                        <p> El coaching es una de las herramientas creadas para movilizar la consciencia personal y grupal a estado de desarrollo que antes no se veían, esto se da a partir del cambio de observador que somos y de esta manera de generar nuevas acciones para crear el futuro que deseamos. El coaching es una de las herramientas creadas para movilizar la consciencia personal y grupal a estado de desarrollo que antes no se veían, esto se da a partir del cambio de observador que somos y de esta manera de generar nuevas acciones para crear el futuro que deseamos.</p>
                        </div>
                 </div>
              </div>
           </div>
         </div>
      </div>
    </div>
  </div>

    </section>
</section>
<!-- About Us And Countdown Area End -->
<!-- Our Blog Area Start -->


<div style="padding-top: 30px;background:#3971bd" class="our-blog-area section-padding-100">
    <div class="container">
        <div class="row">
          <div class="col-12">
              <div class="section-heading-2 text-center wow fadeInUp" data-wow-delay="300ms">
                  
                  <h4>Portafolio</h4>
                  <p style="color:white">Formación de estudio</p>
              </div>
          </div>
                <div class="col-12 col-md-6 col-xl-4">

                    <div style="background-color:#3971bc" class="single-blog-area style-2 wow fadeInUp" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin1.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">Pin Embajador</a>
                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i> Duracion 12 Meses</a>
                                <a class="post-author" href="#"><i class="zmdi zmdi-account"></i> Basico</a>
                            </div>
                            <h2 class="ticket-price"><span>$</span>400 USD</h2>
                            <div class="ticket-pricing-table-details">
                                <p><i class="zmdi zmdi-check"></i> Clases Semi Presenciales</p>
                                <p><i class="zmdi zmdi-check"></i> 4 Profundizaciones</p>
                                <p><i class="zmdi zmdi-check"></i> Aulas Online via zoom</p>
                                <p><i class="zmdi zmdi-check"></i> Acceso a la Universidad del Coach</p>
                                <p><i class="zmdi zmdi-check"></i> Programa de Comisiones 40%</p>
                                <p><i class="zmdi zmdi-check"></i> Acceso a una Convención Mundial (Estadía, Comida y Entrenamiento)</p><br>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <!-- Single Blog Area -->
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="single-blog-area style-2 wow fadeInUp" style="background-color:#3971bc" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin2.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">Pin Director País</a>
                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i>Duracion 40 Meses</a>
                                <a class="post-author" href="#"><i class="zmdi zmdi-account"></i>Avanzado</a>
                            </div>
                            <h2 class="ticket-price"><span>$</span>1500 USD</h2>
                            <div class="ticket-pricing-table-details">
                                <p><i class="zmdi zmdi-check"></i> 5 Certificaciones</p>
                                <p><i class="zmdi zmdi-check"></i> 20 Profundizaciones</p>
                                <p><i class="zmdi zmdi-check"></i> Acceso a la Universidad del Coach</p>
                                <p><i class="zmdi zmdi-check"></i> Programa de Comisiones 60%</p>
                                <p><i class="zmdi zmdi-check"></i> Regalías sobre el 100% de los ingresos anuales de la CMC</p>
                                <p><i class="zmdi zmdi-check"></i> Acceso a 2 Convenciones Mundiales (Santa Marta y Punta Cana)</p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Single Blog Area -->
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="single-blog-area style-2 wow fadeInUp" style="background-color:#3971bc" data-wow-delay="300ms">
                        <!-- Single blog Thumb -->
                        <div class="single-blog-thumb">
                            <img src="<?php echo base_url();?>/assets/plantillaInfo/img/bg-img/pin3.jpg" alt="">
                        </div>
                        <div class="single-blog-text text-center">
                            <a class="blog-title" href="#">½ Pin Director País</a>
                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date" href="#"><i class="zmdi zmdi-alarm-check"></i>Duracion 40 Meses</a>
                                <a class="post-author" href="#"><i class="zmdi zmdi-account"></i>Avanzado</a>
                            </div>
                            <h2 class="ticket-price"><span>$</span>750 USD</h2>
                            <div class="ticket-pricing-table-details">
                                <p><i class="zmdi zmdi-check"></i> 5 Certificaciones</p>
                                <p><i class="zmdi zmdi-check"></i> 20 Profundizaciones</p>
                                <p><i class="zmdi zmdi-check"></i> Acceso a la Universidad del Coach</p>
                                <p><i class="zmdi zmdi-check"></i> Programa de Comisiones 60%</p>
                                <p><i class="zmdi zmdi-check"></i> Regalías sobre el 50% de los ingresos anuales de la CMC</p>
                                <p><i class="zmdi zmdi-check"></i> Acceso a una de las Convenciones Mundiales    (Santa Marta o Punta Cana)</p>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div>
