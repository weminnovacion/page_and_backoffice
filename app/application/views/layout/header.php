<!DOCTYPE html>
<html lang="en">

<head>
    <title>WEMI</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/icono.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins/prism-coy.css?rv=<?= rand(1, 999); ?>">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css?rv=<?= rand(1, 999); ?>">


</head>

<body class="">
    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>

    <!-- [ Pre-loader ] End -->
    <!-- [ navigation menu ] start -->
    <nav class="pcoded-navbar theme-horizontal menu-light">
        <div class="navbar-wrapper container">
            <div class="navbar-content sidenav-horizontal" id="layout-sidenav">
                <ul class="nav pcoded-inner-navbar sidenav-inner" id="menu">
                    <li class="nav-item pcoded-menu-caption">
                        <label>Opciones</label>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->
    <!-- [ Header ] start -->
    <header class="navbar pcoded-header navbar-expand-lg navbar-light header-dark">
        <div class="container">
            <div class="m-header">
                <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
                <a href="#!" class="b-brand">
                    <img src="<?php echo base_url(); ?>assets/images/logo1.png" alt="" style="width: 60%;" id="logo1" class="logo">
                    <img src="<?php echo base_url(); ?>assets/images/logo-icon.png" alt="" class="logo-thumb">
                </a>
                <a href="#!" class="mob-toggler">
                    <i class="feather icon-more-vertical"></i>
                </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <?php
                        include('notification.php');
                    ?>
                    <?php
                        include('profile.php');
                    ?>
                </ul>
            </div>
        </div>
    </header>
    <!-- [ Header ] end -->

    <script>
        var baseurl = "<?php echo base_url(); ?>"
    </script>
    <script src="<?php echo base_url(); ?>assets/js/vendor-all.min.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/ajax/header.js?rv=<?= rand(1, 999); ?>"></script>

    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper container">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">