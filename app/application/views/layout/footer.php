
                            <!-- [ Main Content ] end -->
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- [ Main Content ] end -->
    <script src="<?php echo base_url(); ?>assets/js/vendor-all.min.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap.min.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/pcoded.min.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/apexcharts.min.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/pages/dashboard-main.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/prism.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/horizontal-menu.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/analytics.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/notify/bootstrap-notify.js?rv=<?= rand(1, 999); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/notify/bootstrap-notify.min.js?rv=<?= rand(1, 999); ?>"></script>


    <script>
        $(document).ajaxStart(function () {
            $.blockUI({
                message: `<img src="<?php echo base_url(); ?>assets/images/loading.gif" alt="loading" />`
            });
        });

        $(document).ajaxStop(function () {
            $.unblockUI();
        });

        (function() {
            if ($('#layout-sidenav').hasClass('sidenav-horizontal') || window.layoutHelpers.isSmallScreen()) {
                return;
            }
            try {
                window.layoutHelpers._getSetting("Rtl")
                window.layoutHelpers.setCollapsed(
                    localStorage.getItem('layoutCollapsed') === 'true',
                    false
                );
            } catch (e) {}
        })();

        $(function() {
            $('#layout-sidenav').each(function() {
                new SideNav(this, {
                    orientation: $(this).hasClass('sidenav-horizontal') ? 'horizontal' : 'vertical'
                });
            });
            $('body').on('click', '.layout-sidenav-toggle', function(e) {
                e.preventDefault();
                window.layoutHelpers.toggleCollapsed();
                if (!window.layoutHelpers.isSmallScreen()) {
                    try {
                        localStorage.setItem('layoutCollapsed', String(window.layoutHelpers.isCollapsed()));
                    } catch (e) {}
                }
            });
        });

        $(document).ready(function() {
            $("#pcoded").pcodedmenu({
                themelayout: 'horizontal',
                MenuTrigger: 'hover',
                SubMenuTrigger: 'hover',
            });
        });
    </script>
</body>
</html>