<script>
    var baseurl = "<?php echo base_url(); ?>"
</script>
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>

<style>
#LogoImg2 {
    max-width: 77% !important;
    margin-left: 7% !important;
    padding: 3%;
}

.bt-sesion {
    border-radius: 0 !important;
    height: 68px !important;
    line-height: 68px !important;
}

.ic {
    vertical-align: sub;
    /* vertical-align: middle; */
}
a#nootomColor {
    left: 15%;
}
.classy-nav-container .classy-navbar .classynav ul li.active a {
    color: #0a3e7d !important;
    position: relative !important;top: 50% !important;
}

#colorDiv {
    margin: 1%;
    height: 68px !important;
    max-width: 9%;
}

#conferNav {
    height: 68px !important;
}
.single-ticket-pricing-table .ticket-pricing-table-details p {
    margin-bottom: 0px !important;
}

</style>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>ITC - Invest Trade Company</title>

    <!-- logo -->
    <link rel="icon" href="<?php echo base_url();?>assets/plantillaInfo/./img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plantillaInfo/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/estadisticas/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/estadisticas/css/style.css">
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Header Area Start -->
    <header class="header-area sticky">
        <div class="classy-nav-container breakpoint-on">
            <div class="container-fixed">
                <!-- Classy Menu -->
                <nav class="classy-navbar justify-content-between" id="conferNav">

                    <!-- Logo -->
                    <div class="col-sm-2 classy-navbar" id="colorDiv">
                    <a id="logoa"  href="<?php echo base_url();?>"><img id="LogoImg2" src="<?php echo base_url();?>assets/plantillaInfo/./img/core-img/logo.png" alt=""></a>
                    </div>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler" id="togglerColor">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">
                        <!-- Menu Close Button -->
                        <div class="classycloseIcon">
                              <a><img style="max-width: 107%; position: relative; top: -20px; height: 198px;  width: 320px;" src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/2.jpg" alt=""></a>
                        </div>
                        <!-- Nav Start -->
                        <div class="classynav">
                          <ul class="prueba" id="navColor">
                              <li><a href="<?php echo base_url();?>">INICIO</a></li>
                              <li><a href="#">SERVICIOS</a>
                                <ul class="dropdown" Style="top: 100% !important;">
                                <!-- <li><a href="#">FOREX</a>
                                    <ul class="dropdown" Style="top: 100% !important;"> -->
                                        <li><a href="<?= site_url('C_servicios/serviciosFxtrade'); ?>">FX TRADE</a></li>
                                        <li><a href="<?= site_url('C_servicios/serviciosItmanager'); ?>">IT MANAGER</a></li>
                                    <!-- </ul> -->
                                <!-- </li>
                                <li><a href="#">BINARIAS</a> -->
                                    <!-- <ul class="dropdown" Style="top: 100% !important;"> -->
                                        <li><a href="<?= site_url('C_servicios/serviciosItbinary'); ?>">IT BINARY</a></li>
                                        <li><a href="<?= site_url('C_servicios/serviciosMoneyPro'); ?>">MONEY PRO</a></li>
                                    <!-- </ul> -->
                                <!-- </li>
                                <li><a href="#">OTROS MERCADOS</a>
                                    <ul class="dropdown" Style="top: 100% !important;"> -->
                                    <li><a href="<?= site_url('C_servicios/serviciosCabCapital'); ?>">CAB CAPITAL</a></li>
                                    <!-- </ul> -->
                                <!-- </li> -->
                                </ul>
                              </li>
                              <li><a href="#">EDUCACION</a>
                                <ul class="dropdown" Style="top: 100% !important;">
                                    <li><a href="<?= site_url('C_educacion/educacion'); ?>">TRADING</a></li>
                                    <li><a href="<?= site_url('C_educacion/educacion/#about'); ?>">COACHING</a></li>
                                </ul>
                              </li>
                              <li><a href="#">NOSOTROS</a></li>
                              <li><a href="<?= site_url('C_contacto/index'); ?>">CONTACTO</a></li>
                              <a  href="#" id="nootomColor" class="btn bt-sesion confer-btn mt-3 mt-lg-0 ml-3 ml-lg-5">Iniciar sesion<i class="zmdi zmdi-account-circle zmdi-hc-2x ic"></i></a>
                         
                          </ul>
                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </header>
