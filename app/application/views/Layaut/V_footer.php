<style>
.mfp-image-holder .mfp-content {
    max-width: 50% !important;
}
</style>
<footer class="footer-area bg-img bg-overlay-2 section-padding-100-0">
    <!-- Main Footer Area -->
    <div class="main-footer-area" style="margin-bottom: -4%">
        <div class="container">
            <div class="row">
                <!-- Single Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-60 wow fadeInUp" data-wow-delay="300ms">
                        <!-- Footer Logo -->
                        <a href="#" class="footer-logo"><img src="<?php echo base_url();?>assets/plantillaInfo/img/core-img/logo.png" alt=""></a>
                    
                        <!-- Social Info -->
                        <div class="social-info">
                            <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                            <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                            <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                            <a href="#"><i class="zmdi zmdi-linkedin"></i></a>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-footer-widget mb-60 wow fadeInUp" data-wow-delay="300ms">
                        <!-- Widget Title -->
                        <h5 class="widget-title">Contacto</h5>

                        <!-- Contact Area -->
                        <div class="footer-contact-info">
                            <p><i class="zmdi zmdi-map"></i> Barranquilla (Atlántico)</p>
                            <p><i class="zmdi zmdi-phone"></i> (+ 57) 3016125435</p>
                            <p><i class="zmdi zmdi-email"></i> contacto@investradecompany.com</p>
                            <a href="https://investradecompany.com"><p><i class="zmdi zmdi-globe"></i>investradecompany.com</p></a>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-2">
                    <div class="single-footer-widget mb-60 wow fadeInUp" data-wow-delay="300ms">
                        <!-- Widget Title -->
                        <h5 class="widget-title">Servicios</h5>

                        <!-- Footer Nav -->
                        <ul class="footer-nav">
                            <li><a href="<?= site_url('C_servicios/serviciosFxtrade'); ?>">FX TRADE</a></li>                                
                            <li><a href="<?= site_url('C_servicios/serviciosPamm'); ?>">IT MANAGER</a></li>
                            <li><a href="<?= site_url('C_servicios/serviciosItbinary'); ?>">IT BINARY</a></li>
                            <li><a href="<?= site_url('C_servicios/serviciosFxtrade'); ?>">MONEY PRO</a></li>
                            <li><a href="<?= site_url('C_servicios/serviciosItbinary'); ?>">CAB CAPITAL</a></li>
                        </ul>
                        <!-- Widget Title -->
                        
                    </div>
                </div>

                <!-- Single Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-60 wow fadeInUp" data-wow-delay="300ms">
                    <h5 class="widget-title">Educación</h5>
                        <!-- Footer Nav -->
                        <ul class="footer-nav">
                            <li><a href="<?= site_url('C_servicios/educacion'); ?>">TRADING</a></li>
                            <li><a href="<?= site_url('C_servicios/educacion/#about'); ?>">COACHING</a></li>
                        </ul>
                        <br>
                        <!-- Widget Title -->
                        <h5 class="widget-title">Galeria</h5>

                        <!-- Footer Gallery -->
                        <div class="footer-gallery">
                            <div class="row">
                                <div class="col-4">
                                    <a href="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery1.jpg" class="single-gallery-item"><img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery1.jpg" alt=""></a>
                                </div>
                                <div class="col-4">
                                    <a href="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery2.jpg" class="single-gallery-item"><img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery2.jpg" alt=""></a>
                                </div>
                                <div class="col-4">
                                    <a href="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery3.jpg" class="single-gallery-item"><img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery3.jpg" alt=""></a>
                                </div>
                                <div class="col-4">
                                    <a href="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery4.jpg" class="single-gallery-item"><img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery4.jpg" alt=""></a>
                                </div>
                                <div class="col-4">
                                    <a href="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery5.jpg" class="single-gallery-item"><img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery5.jpg" alt=""></a>
                                </div>
                                <div class="col-4">
                                    <a href="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery6.jpg" class="single-gallery-item"><img src="<?php echo base_url();?>assets/plantillaInfo/img/bg-img/galery6.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Copywrite Area -->
    <div class="container">
        <div class="copywrite-content">
            <div class="row">
                <!-- Copywrite Text -->
                <div class="col-12 col-md-6">
                    <div class="copywrite-text">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script><a href="<?php echo base_url();?>" target="_blank"> Invest Trade Company</a></p>
                    </div>
                </div>
                <!-- Footer Menu -->
                <div class="col-12 col-md-6">
                    <div class="footer-menu">
                        <ul class="nav">
                            <li><a href="#"><i class="zmdi zmdi-circle"></i> Terminos y Condiciones</a></li>
                            <li><a href="#"><i class="zmdi zmdi-circle"></i> Politica de Privacidad</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery 2.2.4 -->
<script src="<?php echo base_url();?>assets/plantillaInfo/js/jquery.min.js"></script>
<!-- Popper -->
<script src="<?php echo base_url();?>assets/plantillaInfo/js/popper.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/plantillaInfo/js/bootstrap.min.js"></script>
<!-- All Plugins -->
<script src="<?php echo base_url();?>assets/plantillaInfo/js/itc.bundle.js"></script>
<!-- Active -->
<script src="<?php echo base_url();?>assets/plantillaInfo/js/default-assets/active.js"></script>
<!-- jQuery Library -->
<script src="<?php echo base_url();?>assets/PlantillaRegistro/js/jquery-3.2.1.min.js"></script>	
<!-- Popper js -->
<script src="<?php echo base_url();?>assets/PlantillaRegistro/js/popper.min.js"></script>
<!-- Bootstrap Js -->
<script src="<?php echo base_url();?>assets/PlantillaRegistro/js/bootstrap.min.js"></script>
<!-- Form Validator -->
<script src="<?php echo base_url();?>assets/PlantillaRegistro/js/validator.min.js"></script>
<!-- Contact Form Js -->
<script src="<?php echo base_url();?>assets/PlantillaRegistro/js/contact-form.js"></script>
<!-- Alertas Js -->  
<script src="<?php echo base_url();?>assets/PlantillaAlerta/bootstrap-notify.js"></script>
<!-- Alertas Js -->  
<script src="<?php echo base_url();?>assets/PlantillaAlerta/bootstrap-notify.min.js"></script>
<!-- Url Alertas Bostrap: http://bootstrap-notify.remabledesigns.com/ -->



  <script src="<?php echo base_url();?>assets/estadisticas/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?php echo base_url();?>assets/estadisticas/js/jquery.waypoints.min.js"></script>
  <script src="<?php echo base_url();?>assets/estadisticas/js/jquery.stellar.min.js"></script>
  <script src="<?php echo base_url();?>assets/estadisticas/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>assets/estadisticas/js/aos.js"></script>
  <script src="<?php echo base_url();?>assets/estadisticas/js/jquery.animateNumber.min.js"></script>
  <script src="<?php echo base_url();?>assets/estadisticas/js/scrollax.min.js"></script>
  <script src="<?php echo base_url();?>assets/estadisticas/js/main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script> -->
</body>
</html>
