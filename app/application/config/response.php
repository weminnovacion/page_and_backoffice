<?php namespace Config;

class ResponseService 
{
	public static function response($module, $message, $data, $status)
    {
        $response['status'] = $status;
        $response['message'] = $message;
        $response['module'] = $module;
        $response['data'] = $data;

        echo json_encode($response);
    }
}
