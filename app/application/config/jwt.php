<?php

namespace Config;

include_once APPPATH . '../vendor/autoload.php';
include_once APPPATH . 'config/jwt.php';
include_once APPPATH . 'config/response.php';

use Config\ResponseService;
use Firebase\JWT\JWT;

class JwtService
{
	public static function getSecretKey()
	{
		return 'weminnovacion.com:2021';
	}

	public static function verifyToken($token)
	{
		try {
			if (!JWT::decode($token, JwtService::getSecretKey(), array('HS256'))) {
				return false;
			} else {
				return true;
			}
		} catch (\Exception $e) {
			return false;
		}
	}
}
