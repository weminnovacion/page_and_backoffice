<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');


class C_registro extends CI_Controller {

    function __Construct(){	
        
        parent::__Construct();
        $this->load->Model('Model_registro');
    }

    function Itmanager()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "fxpamm";
        $data['Registro'] = "Registro Fx Pamm";
        $this->load->view('Registro/V_registro',$data);        
        $this->load->view('Layaut/V_footer');
    }


    function Fxtrade()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "fxtrade";
        $data['Registro'] = "Registro Fx Trade";
        $this->load->view('Registro/V_registro',$data);        
        $this->load->view('Layaut/V_footer');
    }

    function itbinary()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "Itbinary";
        $data['Registro'] = "Registro Itbinary";
        $this->load->view('Registro/V_registro',$data);        
        $this->load->view('Layaut/V_footer');
    }


    function PayuItmanager()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "PayuFxpamm";
        $data['Registro'] = "Registro Fx Pamm";
        $this->load->view('Registro/V_registro',$data);        
        $this->load->view('Layaut/V_footer');
    }

    function PayuFxtrade()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "PayuFxtrade";
        $data['Registro'] = "Registro Fx Trade";
        $this->load->view('Registro/V_registro',$data);        
        $this->load->view('Layaut/V_footer');
    }

    function Payuitbinary()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "Payuitbinary";
        $data['Registro'] = "Registro Itbinary";
        $this->load->view('Registro/V_registro',$data);        
        $this->load->view('Layaut/V_footer');
    }

    function BtcItmanager()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "BtcFxpamm";
        $data['Registro'] = "Registro Fx Pamm";
        $this->load->view('Registro/V_registroItmanagerBtc',$data);        
        $this->load->view('Layaut/V_footer');
    }

    function BtcFxtrade()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "BtcFxtrade";
        $data['Registro'] = "Registro Fx Trade";
        $this->load->view('Registro/V_registroFxtradeBtc',$data);        
        $this->load->view('Layaut/V_footer');
    }

    function Btcitbinary()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "Btcitbinary";
        $data['Registro'] = "Registro Itbinary";
        $this->load->view('Registro/V_registroItbinaryBtc',$data);        
        $this->load->view('Layaut/V_footer');
    }

    function Skrilltmanager()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "SkrillFxpamm";
        $data['Registro'] = "Registro Fx Pamm";
        $this->load->view('Registro/V_registroItmanagerSkrill',$data);        
        $this->load->view('Layaut/V_footer');
    }

    function SkrillFxtrade()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "SkrillFxtrade";
        $data['Registro'] = "Registro Fx Trade";
        $this->load->view('Registro/V_registroFxtradeSkrill',$data);        
        $this->load->view('Layaut/V_footer');
    }

    function Skrillitbinary()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = "Skrillitbinary";
        $data['Registro'] = "Registro Itbinary";
        $this->load->view('Registro/V_registroItbinarySkrill',$data);        
        $this->load->view('Layaut/V_footer');
    }

    public function Registro()
    {
        if ($this->input->is_ajax_request()) {

            $txtname1  = $this->input->post('txtname1');
            $txtapellido1  = $this->input->post('txtapellido1');
            $txtapellido2 = $this->input->post('txtapellido2');
            $txtident  = $this->input->post('txtident');
            $txtemail = $this->input->post('txtemail');
            $txttelefono  = $this->input->post('txttelefono');
            $txtpass = $this->input->post('txtpass');
            $txtpassw  = $this->input->post('txtpassw'); 

            $data = $this->Model_registro->ValidarUserBD($txtident);

            if(!$data){    
                $datosUsua =  array('RolId' => 2,'Username' => $txtident);                
                $UsrId = $this->Model_registro->InsertUsuser($datosUsua);  
                $datosPerso =  array('UserId' => $UsrId,
                                    'PrimerNombre' => $txtname1,
                                    'SegundoNombre' => '',
                                    'PrimerApellido' => $txtapellido1,
                                    'SegundoApellido' => $txtapellido2,
                                    'Telefono' => $txttelefono,
                                    'Correo' => $txtemail,
                                    'TipoCC' => 'CC',
                                    'Identificacion' => $txtident,
                                    'Password' => $txtpassw);
                $UsrId = $this->Model_registro->Insertperso($datosPerso);   
                echo json_encode($UsrId);
            }            
        }
    }
}
