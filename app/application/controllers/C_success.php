<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');


class C_success extends CI_Controller {

    function __Construct(){	parent::__Construct();

        $this->load->Model('Model_products');
    }

    public function index()
    {
        $this->load->helper('url');
 
        $item_number = $_GET['item_number']; 
        $txn_id = $_GET['tx'];
        $payment_gross = $_GET['amt'];
        $currency_code = $_GET['cc'];
        $payment_status = $_GET['st'];

        $array = array(
            'item_number' => $item_number, 
            'tx' => $txn_id,
            'amt' => $payment_gross, 
            'cc' => $currency_code, 
            'st' => $payment_status
        );

        $productResult = $this->Model_products->ListarProductosSuccess($item_number);
        $productRow = $productResult->fetch_assoc();
        $productPrice = $productRow['price'];

        if(!empty($txn_id) && $payment_gross == $productPrice){


            $prevPaymentResult = $this->Model_products->ListarPagos($txn_id);

            if($prevPaymentResult->num_rows > 0){
                $paymentRow = $prevPaymentResult->fetch_assoc();
                $last_insert_id = $paymentRow['payment_id'];
            }else{

                $insert = $this->Model_products->InsertPago($array);
                $last_insert_id = $insert;
            }
            echo "<h1>Your payment has been successful.</h1> <h1>Your Payment ID -'.$last_insert_id.'";

        }else{
            echo '<h1>Your payment has failed.</h1>';
        }
    }
}