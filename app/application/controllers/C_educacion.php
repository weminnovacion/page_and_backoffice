<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class C_educacion extends CI_Controller {

    function __Construct()
    {
      parent::__Construct(); 
    }
    
    function educacion()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $this->load->view('Educacion/V_educacion');
        $this->load->view('Layaut/V_footer');
    }
}
