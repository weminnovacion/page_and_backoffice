<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

include_once APPPATH . '../vendor/autoload.php';
include_once APPPATH . 'config/jwt.php';
include_once APPPATH . 'config/response.php';

use Config\JwtService;
use Config\ResponseService;
use Firebase\JWT\JWT;

class home extends CI_Controller
{
    function __Construct()
    {
        parent::__Construct();
        if (JwtService::verifyToken($this->input->post('token')) == false) redirect('/user/signin', 'refresh');
        $this->load->Model('Model_home');
    }

    public function index(){
        
        
    }
}