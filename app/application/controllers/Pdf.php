<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

include_once APPPATH . 'libraries/third_party/vendor/autoload.php';

class pdf extends CI_Controller
{

    function __Construct()
    {
        parent::__Construct();
    }

    public function descargar()
    {
        $mpdf = new \Mpdf\Mpdf();
        $data = array();
        $pdfcontent = '<h1>Hola este es el pdf de prueba</h1>';

        $mpdf->WriteHTML($pdfcontent);

        $mpdf->SetDisplayMode('fullpage');
        $mpdf->list_indent_first_level = 0;

        //call watermark content and image
        $mpdf->SetWatermarkText('WEMI INNOVACION Y DESARROLLO');
        $mpdf->showWatermarkText = true;
        $mpdf->watermarkTextAlpha = 0.1;

        //output in browser
        $mpdf->Output();
    }
}
