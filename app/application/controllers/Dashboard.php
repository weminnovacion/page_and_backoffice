<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

include_once APPPATH . '../vendor/autoload.php';
include_once APPPATH . 'config/jwt.php';

use Config\JwtService;

class dashboard extends CI_Controller
{

  function __Construct()
  {
    parent::__Construct();
    $rest = JwtService::verifyToken($this->session->userdata('token') ? $this->session->userdata('token') : $this->input->post('token'));
    if (!$rest) redirect('/user/signin', 'refresh');
  }

  public function index()
  {
    $this->load->helper('url');
    $this->load->view('layout/header');
    $this->load->view('layout/page_header');
    $this->load->view('dashboard/index');
    $this->load->view('layout/footer');
  }
}
