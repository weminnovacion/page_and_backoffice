<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

include_once APPPATH . '../vendor/autoload.php';
include_once APPPATH . 'config/jwt.php';
include_once APPPATH . 'config/response.php';

use Config\JwtService;
use Config\ResponseService;

class header extends CI_Controller
{
    function __Construct()
    {
        parent::__Construct();
        $rest = JwtService::verifyToken($this->session->userdata('token') ? $this->session->userdata('token') : $this->input->post('token'));
        if (!$rest) redirect('/user/signin', 'refresh');

        $this->load->Model('Model_header');
    }

    public function index()
    {
        $listMenuFather = $this->Model_header->listMenuFather();
        $listMenuChild = $this->Model_header->listMenuChild();

        $res = array('arrayMenuFather' => $listMenuFather, 'arrayMenusChild' => $listMenuChild);
        return ResponseService::response('menu', 'index', $res, 'success');
    }
}
