<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

include_once APPPATH . '../vendor/autoload.php';
include_once APPPATH . 'config/jwt.php';
include_once APPPATH . 'config/response.php';

use Config\JwtService;
use Config\ResponseService;
use Firebase\JWT\JWT;

class user extends CI_Controller
{
    function __Construct()
    {
        parent::__Construct();
        $this->load->Model('Model_signin');
    }

    public function index()
    {
        $rest = JwtService::verifyToken($this->session->userdata('token'));
        if ($rest == true) {
            redirect('/dashboard', 'refresh');
        } else {
            redirect('/user/signin', 'refresh');
        }
    }

    public function signin()
    {
        $this->load->helper('url');
        $this->load->view('signin/index');
    }

    public function post_signin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($username != null && $password != null) {

            $data = $this->Model_signin->validUser($username);
            if (!$data) return ResponseService::response('signin', "El usuario: $username no existe en la base de datos", null, 'error');
            if (!password_verify($password, $data[0]->password)) return ResponseService::response('signin', "contaseña incorrecta", null, 'error');

            $key = JwtService::getSecretKey();
            $payload = [
                'aud' => 'https://weminnovacion.com',
                'iat' => 1356999524,
                'nbf' => 1357000000,
                'username' => $username
            ];

            $jwt = JWT::encode($payload, $key);

            $data = array(
                'token' => $jwt,
                'name' => $username
            );

            $this->session->set_userdata($data);

            return ResponseService::response('signin', 'login', $jwt, 'success');
        } else {
            return ResponseService::response('signin', 'Llenar los campos requeridos', null, 'error');
        }
    }
}
