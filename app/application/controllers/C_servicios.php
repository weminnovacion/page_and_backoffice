<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class C_servicios extends CI_Controller {

    function __Construct()
    {
      parent::__Construct();
      $this->load->Model('Model_registro');
      $this->load->Model('Model_validation');
      $this->load->Model('Model_header');  
    }
 
    function serviciosItmanager()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $this->load->view('Servicios/V_serviciosItmanager');
        $this->load->view('Layaut/V_footer');
    }
    
    function serviciosItbinary()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $this->load->view('Servicios/V_serviciosItbinary');
        $this->load->view('Layaut/V_footer');
    }
    
    function serviciosFxtrade()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $this->load->view('Servicios/V_serviciosFxtrade');
        $this->load->view('Layaut/V_footer');
    }	

    function serviciosMoneyPro()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $this->load->view('Servicios/V_serviciosMoneyPro');
        $this->load->view('Layaut/V_footer');
    }
    
    function serviciosCabCapital()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $this->load->view('Servicios/V_serviciosCabCapital');
        $this->load->view('Layaut/V_footer');
    }
}
