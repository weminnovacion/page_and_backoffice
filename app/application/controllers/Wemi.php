<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

include_once APPPATH . '../vendor/autoload.php';
include_once APPPATH . 'config/jwt.php';
include_once APPPATH . 'config/response.php';

use Config\JwtService;
use Config\ResponseService;

class wemi extends CI_Controller
{
    function __Construct()
    {
        parent::__Construct();
        $rest = JwtService::verifyToken($this->session->userdata('token') ? $this->session->userdata('token') : $this->input->post('token'));
        if (!$rest) redirect('/user/signin', 'refresh');
        
        $this->load->Model('Model_wemi');
    }

    public function header()
    {
        $this->load->helper('url');
        $this->load->view('layout/header');
        $this->load->view('layout/page_header');
        $this->load->view('wemi/header');
        $this->load->view('layout/footer');
    }

    public function getHeader(){
        $headers = $this->Model_wemi->getHeader();  
        $links = $this->Model_wemi->getLink();  
        $menus = $this->Model_wemi->getMenu();  

        $res = array('headers' => $headers, 'links' => $links, 'menus' => $menus);

        return ResponseService::response('header', 'Registrado con exito', $res, 'success');
    }

    public function post_header()
    {
        $name = $this->input->post('name');
        $hour = $this->input->post('hour');
        $whatsapp = $this->input->post('whatsapp');
        $phone = $this->input->post('phone');
        $links = $this->input->post('links');
        $menus = $this->input->post('menus');

        if ($name != null) {

            $this->Model_wemi->delete();      
            $datos =  array(
                'name' => $name,
                'hour' => $hour,
                'whatsapp' => $whatsapp,
                'phone' => $phone
            );
            $this->Model_wemi->post($datos);   
            
            $this->Model_wemi->deleteLink();
            foreach ($links as $a) {
                $datosLink =  array(
                    'link' => $a['value']
                );
                
                $this->Model_wemi->postLink($datosLink);   
            }

            $this->Model_wemi->deleteMenu();
            foreach ($menus as $a) {
                $datosMenu =  array(
                    'value' => $a['value']
                );
                
                $this->Model_wemi->postMenu($datosMenu);   
            }

            return ResponseService::response('header', 'Registrado con exito', $datos, 'success');
        } else {
            return ResponseService::response('signin', 'Llenar los campos requeridos', null, 'error');
        }
    }

    public function post_icon(){
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 2048;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto')) { #Aquí me refiero a "foto", el nombre que pusimos en FormData
            $error = array('error' => $this->upload->display_errors());
            return ResponseService::response('header', 'Error al subir la imagen', $error, 'error');
        } else {
            return ResponseService::response('header', 'imagen subida', null, 'success');
        }
    }
}
