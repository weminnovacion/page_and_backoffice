<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class C_header extends CI_Controller
{
    function __Construct()
    {
       parent::__Construct();
       $this->load->Model('Model_header');
    }

    public function ListarMenus(){
        $siglacargo = 'ROL_VISITANTE';
        $data = $this->Model_header->ListarMenus($siglacargo);
        $data2 = $this->Model_header->ListarMenusHijo2($siglacargo);
        $resultado = array('arrayMenus' => $data, 'arrayMenusH2' => $data2);
        echo json_encode( $resultado);
	}

	public function ListarMenusHijo2(){
      $siglacargo = 'ROL_VISITANTE';
      $data = $this->Model_header->ListarMenus($siglacargo);
	    echo json_encode($data);
  }
  
}
