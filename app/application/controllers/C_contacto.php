<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');


class C_contacto extends CI_Controller {

    function __Construct(){	parent::__Construct();
        $this->load->Model('Model_header');
        $this->load->Model('Model_contacto');
    }



    public function index()
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $this->load->view('Contacto/V_contacto');
        $this->load->view('Layaut/V_footer');
    }

    public function SendMail()
    {

        $data = $this->input->post();
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');

        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'contacto.itcompany@gmail.com';
        $mail->Password = 'Dios0809*';
        $mail->SMTPSecure = 'tls';
        $mail->Port     = 587;

        $mail->setFrom('contact@investradecompany.com', 'ITC(Invest Trade Company)');
        $mail->addReplyTo('contact@investradecompany.com', 'ITC(Invest Trade Company)');

        // Add a recipient
        $mail->addAddress('contacto@investradecompany.com');

        // Add cc or bcc
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        // Email subject
        $mail->Subject = 'Bienvenidos a ITC(Invest Trade Company)';

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = "<h1>Bienvenidos a ITC(Invest Trade Company)</h1>
            <p>Prueba Correo funcional de la compañia mas prestigiosa de los ultimos tiempos</p>";
        $mail->Body = $mailContent;

        // Send email
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
            //Guardar correo en base de datos
             $info =  array(
                     'idcorreo' => '',
                     'nombre' => $data["nombre"],
                     'apellido' => $data["apellido"],
                     'correo' => $data["email"],
                     'telefono' => $data["telefono"],
                     'mensaje' => $data["mensaje"]
                    );

                    $this->Model_contacto->insertIntoCorreo($info);
            }
    }
}
