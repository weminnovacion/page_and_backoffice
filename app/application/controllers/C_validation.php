<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');


class C_validation extends CI_Controller {

    function __Construct(){	parent::__Construct();

        $this->load->Model('Model_validation');
    }
}