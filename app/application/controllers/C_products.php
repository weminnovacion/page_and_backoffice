<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class C_products extends CI_Controller {

    function __Construct()
    {
      parent::__Construct();
      $this->load->Model('Model_products');
      $this->load->Model('Model_header');  
    }
  
    function indexProductos($txtTipoServicio)
    {
        $this->load->helper('url');
        $this->load->view('Layaut/V_header');
        $data['TipoServicio'] = $txtTipoServicio;
        $this->load->view('Productos/V_products',$data);
        $this->load->view('Layaut/V_footer');
    }

    public function listarProductos()
    {

      if ($this->input->is_ajax_request()) {

        $txtTipoServicio = $this->input->post('txtTipoServicio');
        $data = $this->Model_products->ListarProductos($txtTipoServicio);
        echo json_encode($data);          
      }
    }
}
