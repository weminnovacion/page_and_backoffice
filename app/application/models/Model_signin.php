<?php

class Model_signin extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function validUser($username)
    {
        $this->db->select('*');
        $this->db->from('uweminn6_wemi_dv.user');
        $this->db->where('username', $username);
        $this->db->where('status', 1);
        $datos = $this->db->get();
        return $datos->result();
    }
}
