<?php

class Model_wemi extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function getHeader()
    {
        $this->db->select('m.*');
        $this->db->from('uweminn6_wemi_dv.we_header m');
        $datos = $this->db->get();
        return $datos->result();
    }

    public function post($datos){
        $this->db->insert('uweminn6_wemi_dv.we_header',$datos);
        return $this->db->insert_id();
    }

    public function delete(){
        return $this->db->empty_table('uweminn6_wemi_dv.we_header');
	}

    public function getLink()
    {
        $this->db->select('m.*');
        $this->db->from('uweminn6_wemi_dv.we_link m');
        $datos = $this->db->get();
        return $datos->result();
    }

    public function deleteLink(){
        return $this->db->empty_table('uweminn6_wemi_dv.we_link');
    }

    public function postLink($datos){
        $this->db->insert('uweminn6_wemi_dv.we_link',$datos);
        return $this->db->insert_id();
    }

    public function getMenu()
    {
        $this->db->select('m.*');
        $this->db->from('uweminn6_wemi_dv.we_menu m');
        $datos = $this->db->get();
        return $datos->result();
    }

    public function deleteMenu(){
        return $this->db->empty_table('uweminn6_wemi_dv.we_menu');
    }

    public function postMenu($datos){
        $this->db->insert('uweminn6_wemi_dv.we_menu',$datos);
        return $this->db->insert_id();
    }
}
