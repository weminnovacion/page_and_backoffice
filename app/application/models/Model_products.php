<?php

class Model_products extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    public function ListarProductos($txtTipoServicios1){
        $this->db->select('*');
        $this->db->from('u112283771_bditc.products');
        $this->db->where('tipoServicio',$txtTipoServicios1);
        $datos = $this->db->get();
        return $datos->result();
    }

    public function ListarProductosFxtrade(){
        $this->db->select('*');
        $this->db->from('u112283771_bditc.products');
        $this->db->where('tipoServicio','fxtrade');
        $datos = $this->db->get();
        return $datos->result();
    }

    public function ListarProductosItbinary(){
        $this->db->select('*');
        $this->db->from('u112283771_bditc.products');
        $this->db->where('tipoServicio','itbinary');
        $datos = $this->db->get();
        return $datos->result();
    }

    public function ListarProductosSuccess($item_number){
        $this->db->select('price');
        $this->db->from('u112283771_bditc.products');
        $this->db->where('id', $item_number);
        $datos = $this->db->get();
        return $datos->result();
    }

    public function ListarPagos($txn_id){
        $this->db->select('payment_id');
        $this->db->from('u112283771_bditc.payments');
        $this->db->where('txn_id', $txn_id);
        $datos = $this->db->get();
        return $datos->result();
    }

    public function InsertPago($datos){
        $this->db->insert('u112283771_bditc.itc_persona',$datos);
        return $this->db->insert_id();
	}


}
