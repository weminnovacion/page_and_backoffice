<?php

class Model_registro extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

   	public function InsertUsuser($datos){
        
        $this->db->insert('itc_usuario',$datos);
        return $this->db->insert_id();
    }
    
    public function Insertperso($datos){
        
        $this->db->insert('itc_persona',$datos);
        return $this->db->insert_id();
	}

	public function ValidarUserBD($identificacion){
        $this->db->select('*');
        $this->db->from('u112283771_bditc.itc_persona');
        $this->db->where('Identificacion',$identificacion);
        $datos = $this->db->get();
        return $datos->result();
    }
}
