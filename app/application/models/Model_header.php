<?php

class Model_header extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function listMenuFather()
    {
        $this->db->select('id, father_id, type_menu_id, name, icon, controller, method, (select COUNT(m2.id) from menu m2 WHERE m2.father_id = m.id AND m2.type_menu_id = 2) as count');
        $this->db->from('uweminn6_wemi_dv.menu m');
        $this->db->where('m.eliminado', 0);
        $this->db->where('m.type_menu_id', 1);
        $datos = $this->db->get();
        return $datos->result();
    }

    public function listMenuChild()
    {
        $this->db->select('id, father_id, type_menu_id, name, icon, controller, method');
        $this->db->from('uweminn6_wemi_dv.menu');
        $this->db->where('eliminado', 0);
        $this->db->where('type_menu_id', 2);
        $datos = $this->db->get();
        return $datos->result();
    }
}
